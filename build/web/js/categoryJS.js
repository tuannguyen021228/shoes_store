/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
$(document).ready(function() {
    $(document).on("click", "#categoryBtn", function() {
        $("#form_category").validate({
            rules: {
                categoryName: {
                    required: true,
                    minlength: 2,
                    maxlength: 50
                }
            },
            messages: {
                categoryName: {
                    required: "Enter brand to add!",
                    minlength: "You must input 2 characters at least",
                    maxlength: "Category name must less than 50 characters"
                }
            }
        });
        $("#form_category").valid();
    });
});

