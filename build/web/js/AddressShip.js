$(document).ready(function() {
    $(document).on("click", "#addAddressShip", function() {
        $("#form_address").validate({
            rules: {
                addressName: {
                    required: true,
                    minlength: 4,
                    maxlength: 50
                }
            },
            messages: {
                addressName: {
                    required: "Enter your address!",
                    minlength: "You must input 4 characters at least",
                    maxlength: "Your address must less than 50 characters"
                }
            }
        });
        $("#form_address").valid();
    });
});