/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
$(document).ready(function() {
    $(document).on("click", "#brandBtn", function() {
        $("#form_brand").validate({
            rules: {
                brandName: {
                    required: true,
                    minlength: 2,
                    maxlength: 50
                }
            },
            messages: {
                brandName: {
                    required: "Enter brand to add!",
                    minlength: "You must input 2 characters at least",
                    maxlength: "Brand name must less than 50 characters"
                }
            }
        });
        $("#form_brand").valid();
    });
});

