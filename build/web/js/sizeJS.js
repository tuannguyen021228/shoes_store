/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
$(document).ready(function() {
    $(document).on("click", "#sizeBtn", function() {
        $("#form_size").validate({
            rules: {
                sizes: {
                    required: true,
                    minlength: 2,
                    maxlength: 4
                }
            },
            messages: {
                sizes: {
                    required: "Enter sizes to add!",
                    minlength: "You must input 2 characters at least",
                    maxlength: "size must in range [2,4]"
                }
            }
        });
        $("#form_size").valid();
    });
});

