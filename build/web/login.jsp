<%-- 
    Document   : login
    Created on : Sep 12, 2022, 6:18:53 PM
    Author     : TuaniuLinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Art shoes store</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- VALIDATION PLUGIN -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <!-- ICON -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
        
        <link rel="stylesheet" href="css/login.css"/>
        <script src="js/loginJS.js"></script>
    </head>
    <body>
        <style>
            @font-face {
                font-family: "BLKCHCRY";
                src: url(font/BLKCHCRY.TTF);
            }
            
            #sign_in {
                font-family: "BLKCHCRY", sans-serif;
            }
        </style>
        <div class="container">
            <div class="">
                <div class="row shadow-lg justify-content-center">
                    <div class="col-lg-4 col-md-6" style="padding: 0;">
                        <img id="img" class="rounded-start" src="img/Shoe_Login.jpg" alt="shoe">
                    </div>
                    <div id="form_login" class="col-lg-8 col-md-6 px-5 rounded-end">
                        <div class="pt-5">
                            <h1 class="text-center my-4" id="sign_in">Sign In</h1>
                        </div>
                        <form id="form_jquery" action="login" method="post">
                            <div class="form-group mb-4">
                                <label class="mb-2 fw-bold" for="userName">User name</label>
                                <input type="text" class="form-control p-3" id="userName" name="userName" placeholder="Username">
                            </div>
                            <div class="form-group mb-4">
                                <label class="mb-2 fw-bold" for="password">Password</label>
                                <input type="password" class="form-control p-3" id="password" name="password" placeholder="Password">
                            </div>
                            <div class="form-group mb-4">
                                <button id="login" type="submit" class="p-3 form-control btn btn-lg text-white"> Login
                                </button>
                            </div>
                            <div class="text-center pt-4 mb-5">You not have an account? <a class="text-decoration-none" href="register"> Sign Up</a></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="text-center mt-5">
                <p class="text-white">Copyright © 2022 ArtShoeStore Inc. All rights reserved.</p>
                <p class="text-white">Developer: TuanNB</p>
                <div class="mt-4 mb-5 pb-5">
                    <a href="https://www.facebook.com/tuannguyen281202/"><i style="font-size: 40px;" class="fa-brands fa-facebook"></i></a>
                    <a class="ms-5" href="https://www.instagram.com/kint.eagle/"><i style="font-size: 40px;" class="fa-brands fa-instagram"></i></a>
                    <a class="ms-5" href="https://twitter.com/k16_hl"><i style="font-size: 40px;" class="fa-brands fa-twitter"></i></a>
                </div>
            </div>
        </div>
    </body>

</html>
