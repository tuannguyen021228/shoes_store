<%-- 
    Document   : header
    Created on : Oct 6, 2022, 5:14:56 PM
    Author     : TuaniuLinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false"%>
<link rel="stylesheet" href="css/all.css"/>
<!-- BEGIN HEADER -->
<div id="header" class="py-3">
    <a href="home" class="text-decoration-none"><div class="ms-4 mt-1" id="logo">ArtShoes</div></a>
    <div id="form_search">
        <form action="home" method="post">
            <div class="input-group">
                <input type="text" class="form-control form-control-lg" name="shoesNameSearch" placeholder="Search By Name">
                <button type="submit" class="input-group-text btn-success"><i class="bi bi-search"></i>Search</button>
            </div>
        </form>
    </div>
    <div id="user_logo" class="me-5 mt-2">
        <c:choose>
            <c:when test="${sessionScope.account != null}">
                <c:choose>
                    <c:when test="${sessionScope.account.role eq 'member'}">
                        <div class="me-4">
                            <a class=" nav-link text-white mt-1" href="cart"><i class="fa-solid fa-cart-shopping"></i> Cart shopping</a>
                        </div>
                        <div class="me-4">
                            <a class="nav-link text-white mt-1" href="UserProfile?action=view"><i class="fa-solid fa-user"></i> Welcome, ${sessionScope.account.userName}</a>
                        </div>
                        <div>
                            <a class="nav-link text-white mt-1" href="login?action=logout"><i class="fa-solid fa-right-from-bracket"></i> Logout</a>
                        </div>
                    </c:when>
                    <c:otherwise>
                        <div class="me-4">
                            <a class="nav-link text-white mt-1" href="Statistic"><i class="fa-solid fa-user"></i> Welcome, Admin</a>
                        </div>
                        <span class="text-light">
                            |
                        </span>
                        <div class="ms-4">
                            <a class="nav-link text-white mt-1" href="login?action=logout"><i class="fa-solid fa-right-from-bracket"></i> Logout</a>
                        </div>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <div class="me-4">
                    <a class="nav-link text-white mt-1" href="register"><i class="fa-solid fa-registered"></i> Register</a>
                </div>
                <span class="text-light">
                    |
                </span>
                <div class="ms-4">
                    <a class="nav-link text-white mt-1" href="login"><i class="fa-solid fa-right-to-bracket"></i> Login</a>
                </div>
            </c:otherwise>
        </c:choose>
    </div>
</div>
<!-- END HEADER -->
