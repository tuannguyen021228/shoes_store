/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import accountDao.CartDao;
import accountDao.ColorDao;
import accountDao.GenderDao;
import accountDao.QuantityDao;
import accountDao.ShoesDao;
import accountDao.ShoesGenderDao;
import accountDao.SizeDao;
import entities.Account;
import entities.Cart;
import entities.Color;
import entities.Gender;
import entities.Quantity;
import entities.Shoes;
import entities.ShoesGender;
import entities.Size;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
@WebServlet(name = "shoesDetail", urlPatterns = {"/shoesDetail"})
public class shoesDetailController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet shoesDetailController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet shoesDetailController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String idRaw = request.getParameter("shoesId");
        String colorIdRaw = request.getParameter("colorId");
        String sizeIdRaw = request.getParameter("sizeId");
        HttpSession session = request.getSession();
        List<Gender> listGender = new ArrayList<Gender>();
        List<Color> colors = new ArrayList<Color>();
        List<Size> sizes = new ArrayList<Size>();
        try {
            int id = Integer.parseInt(idRaw);
            int colorId = Integer.parseInt(colorIdRaw);
            int sizeId = Integer.parseInt(sizeIdRaw);
            int quan = 0;
            ColorDao colorDao = new ColorDao();
            SizeDao sizeDao = new SizeDao();
            ShoesDao shoesDao = new ShoesDao();
            ShoesGenderDao shoesGenderDao = new ShoesGenderDao();
            GenderDao genderDao = new GenderDao();
            QuantityDao quantityDao = new QuantityDao();
            
            Shoes shoes = shoesDao.getShoesById(id);
            List<ShoesGender> listSG = shoesGenderDao.getGenderByShoesId(id);
            for (ShoesGender sg : listSG) {
                listGender.add(genderDao.getGenderByShoesId(sg));
            }

            List<Quantity> listQuantity = quantityDao.getQuantity(id);
            
            for (Quantity q : listQuantity) {
                if (colors.isEmpty()) {
                    colors.add(colorDao.getByColorId(q.getColorId()));
                } else {
                    Color color = colorDao.getByColorId(q.getColorId());
                    int check = 0;
                    for (Color c : colors) {
                        if (color.getColorId() == c.getColorId()) {
                            ++check;
                        }
                    }
                    if (check == 0) {
                        colors.add(color);
                    }
                }
                if (sizes.isEmpty()) {
                    sizes.add(sizeDao.getBySizeId(q.getSizeId()));
                } else {
                    Size size = sizeDao.getBySizeId(q.getSizeId());
                    int check = 0;
                    for (Size s : sizes) {
                        if (size.getSizeId() == s.getSizeId()) {
                            ++check;
                        }
                    }
                    if (check == 0) {
                        sizes.add(size);
                    }
                }
                quan += q.getQuantity();
            }
            
            if(colorId > 0 && sizeId > 0){
                quan = 0;
                for(Quantity q : listQuantity){
                    if(q.getColorId() == colorId && q.getSizeId() == sizeId){
                        quan += q.getQuantity();
                    }
                }
            }else if(colorId > 0){
                quan = 0;
                for(Quantity q : listQuantity){
                    if(q.getColorId() == colorId){
                        quan += q.getQuantity();
                    }
                }
            }else if(sizeId > 0){
                quan = 0;
                for(Quantity q : listQuantity){
                    if(q.getSizeId()== sizeId){
                        quan += q.getQuantity();
                    }
                }
            }
            session.setAttribute("colorId", colorId);
            session.setAttribute("sizeId", sizeId);
            session.setAttribute("shoesDetail", shoes);
            session.setAttribute("genders", listGender);
            session.setAttribute("colors", colors);
            session.setAttribute("quantities", listQuantity);
            session.setAttribute("sizes", sizes);
            session.setAttribute("quan", quan);
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("shoesDetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
