/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import accountDao.BrandDao;
import entities.Brand;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
@WebServlet(name="Brand", urlPatterns={"/Brand"})
public class BrandController extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BrandController</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BrandController at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        BrandDao bd = new BrandDao();
        List<Brand> list;
        try {
            list = bd.getAll();
            if (list.isEmpty()) {
                session.setAttribute("listBrand", null);
            } else {
                session.setAttribute("listBrand", list);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        response.sendRedirect("AdminDashboard?action=addBrand");
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        String brandName = request.getParameter("brandName");
        BrandDao bd = new BrandDao();
        Brand brand = new Brand(brandName.toLowerCase());
        try {
            int check = 0;
            List<Brand> list = bd.getAll();
            for (Brand b : list) {
                if (b.getBrandName().equals(brand.getBrandName())) {
                    ++check;
                }
            }
            if (check > 0) {
                session.setAttribute("errorBrand", "Brand already existed!");
            } else {
                bd.create(brand);
                session.setAttribute("errorBrand", null);
            }
        } catch (Exception e) {
            session.setAttribute("errorBrand", "An error occured!");
        }
        response.sendRedirect("Brand");
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
