/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import accountDao.AddressShipDao;
import entities.Account;
import entities.AddressShip;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author TuaniuLinh
 */
@WebServlet(name = "AddressShip", urlPatterns = {"/AddressShip"})
public class AddressShipController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddressShip</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddressShip at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        AddressShipDao addressShipDao = new AddressShipDao();
        List<AddressShip> list;
        try {
            Account acc = (Account) session.getAttribute("account");
            list = addressShipDao.getListAddressShip(acc);
            if (list.isEmpty()) {
                session.setAttribute("listAddress", null);
            } else {
                session.setAttribute("listAddress", list);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        response.sendRedirect("UserProfile?action=shipAddress");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String addressName = request.getParameter("addressName");
        HttpSession session = request.getSession();
        Account account = (Account) session.getAttribute("account");
        AddressShipDao addressShipDao = new AddressShipDao();
        AddressShip addressShip = new AddressShip(account.getAccountId(), addressName);
        try {
            addressShipDao.create(addressShip);
            response.sendRedirect("AddressShip");
        } catch (Exception e) {
            response.sendRedirect("AddressShip");
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
