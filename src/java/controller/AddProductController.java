/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import accountDao.BrandDao;
import accountDao.CategoryDao;
import accountDao.ColorDao;
import accountDao.GenderDao;
import accountDao.GenerationDao;
import accountDao.QuantityDao;
import accountDao.ShoesDao;
import accountDao.ShoesGenderDao;
import accountDao.SizeDao;
import entities.Brand;
import entities.Category;
import entities.Color;
import entities.Gender;
import entities.Generation;
import entities.Quantity;
import entities.Shoes;
import entities.ShoesGender;
import entities.Size;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
@WebServlet(name = "AddProduct", urlPatterns = {"/AddProduct"})
public class AddProductController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddProductController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddProductController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        CategoryDao categoryDao = new CategoryDao();
        BrandDao brandDao = new BrandDao();
        GenerationDao generationDao = new GenerationDao();
        ColorDao colorDao = new ColorDao();
        SizeDao sizeDao = new SizeDao();
        GenderDao genderDao = new GenderDao();
        try {
            List<Category> categories = categoryDao.getAll();
            List<Brand> brands = brandDao.getAll();
            List<Generation> generations = generationDao.getAll();
            List<Color> colors = colorDao.getAll();
            List<Size> sizes = sizeDao.getAll();
            List<Gender> genders = genderDao.getAll();
            session.setAttribute("categories", categories);
            session.setAttribute("brands", brands);
            session.setAttribute("generations", generations);
            session.setAttribute("colors", colors);
            session.setAttribute("sizes", sizes);
            session.setAttribute("genders", genders);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        response.sendRedirect("AdminDashboard?action=addProduct");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String shoesName = request.getParameter("shoesName");
        String price_raw = request.getParameter("price");
        String quantity_raw = request.getParameter("quantity");
        String description = request.getParameter("description");
        String image = request.getParameter("image");
        String category = request.getParameter("categoryId");
        String brand = request.getParameter("brandId");
        String generation = request.getParameter("generationId");
        String[] colors = request.getParameterValues("colorId");
        String[] sizes = request.getParameterValues("sizeId");
        String[] genders = request.getParameterValues("genderId");
        Shoes shoes;
        ShoesDao shoesDao = new ShoesDao();
        int checkQuantity = -1;
        int newQuantity = 0;
        try {
            double price = Double.parseDouble(price_raw);
            int quantity = Integer.parseInt(quantity_raw);
            int categoryId = Integer.parseInt(category);
            int brandId = Integer.parseInt(brand);
            int generationId = Integer.parseInt(generation);
            shoes = new Shoes(categoryId, brandId, generationId, shoesName, price, description, image);
            int kindOfShoes = colors.length * sizes.length;
            checkQuantity = quantity % kindOfShoes;
            newQuantity = quantity / kindOfShoes;
            int check = 0;
            List<Shoes> list = shoesDao.getAll();
            for (Shoes s : list) {
                if (s.getShoesName().equals(shoes.getShoesName())) {
                    ++check;
                }
            }
            if (check > 0) {
                session.setAttribute("errorProduct", "Product already existed!");
            } else {
                shoesDao.create(shoes);
                
                session.setAttribute("errorProduct", null);
            }
        } catch (Exception e) {
            session.setAttribute("errorProduct", "An error occured while add shoes!");
        }

        if (checkQuantity == 0) {
            try {
                int quantity = Integer.parseInt(quantity_raw);
                shoes = shoesDao.getShoesByName(shoesName);
                QuantityDao quantityDao = new QuantityDao();
                ShoesGenderDao shoesGenderDao = new ShoesGenderDao();
                for (String c : colors) {
                    for (String s : sizes) {
                        int colorId = Integer.parseInt(c);
                        int sizeId = Integer.parseInt(s);
                        Quantity q = new Quantity(shoes.getShoesId(), sizeId, colorId, newQuantity);
                        quantityDao.create(q);
                    }
                }
                for (String g : genders) {
                    int genderId = Integer.parseInt(g);
                    ShoesGender sg = new ShoesGender(shoes.getShoesId(), genderId);
                    shoesGenderDao.create(sg);
                }
                
            } catch (Exception e) {
                session.setAttribute("errorProduct", "An error occured while add quantity or gender!");
            }
        }

        response.sendRedirect("AddProduct");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
