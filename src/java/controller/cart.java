/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller;

import accountDao.AccountDao;
import accountDao.AddressShipDao;
import accountDao.CartDao;
import accountDao.ColorDao;
import accountDao.ShoesDao;
import accountDao.SizeDao;
import entities.Account;
import entities.Cart;
import entities.Color;
import entities.Item;
import entities.Shoes;
import entities.Size;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
@WebServlet(name="cart", urlPatterns={"/cart"})
public class cart extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet cart</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet cart at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession();
        double total = 0;
        try {
            AccountDao accountDao = new AccountDao();
            Account account = (Account) session.getAttribute("account");
            AddressShipDao addressShipDao = new AddressShipDao();
            CartDao cartDao = new CartDao();
            ShoesDao shoesDao = new ShoesDao();
            ColorDao colorDao = new ColorDao();
            SizeDao sizeDao = new SizeDao();
            List<Cart> carts = cartDao.getAll();
            List<Item> items = new ArrayList<Item>();
            for(Cart c : carts){
                Shoes shoes = shoesDao.getShoesById(c.getShoesId());
                Color color = colorDao.getByColorId(c.getColorId());
                Size size = sizeDao.getBySizeId(c.getSizeId());
                Item item = new Item(c.getCartId(), shoes, color, size, c.getQuantity());
                total += shoes.getPrice() * c.getQuantity();
                items.add(item);
            }
            session.setAttribute("itemsSize", items.size());
            session.setAttribute("addressShips", addressShipDao.getListAddressShip(account));
            session.setAttribute("items", items);
            session.setAttribute("total", total);
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("cart.jsp").forward(request, response);
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
