/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import accountDao.SizeDao;
import entities.Size;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
@WebServlet(name = "Size", urlPatterns = {"/Size"})
public class SizeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SizeController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SizeController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        SizeDao sizeDao = new SizeDao();
        List<Size> listSize;
        try {
            listSize = sizeDao.getAll();
            if (listSize.isEmpty()) {
                session.setAttribute("listSize", null);
            } else {
                session.setAttribute("listSize", listSize);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect("AdminDashboard?action=addSize");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        String size_raw = request.getParameter("sizes");
        SizeDao sizeDao = new SizeDao();
        Size size;
        try {
            double sizes = Double.parseDouble(size_raw);
            size = new Size(sizes);
            List<Size> list = sizeDao.getAll();
            int check = 0;
            for (Size s : list) {
                if (s.getSizes() == size.getSizes()) {
                    ++check;
                }
            }
            if (check > 0) {
                session.setAttribute("errorSize", "Size already existed!");
            } else {
                sizeDao.create(size);
                session.setAttribute("errorSize", null);
            }
        } catch (Exception e) {
            session.setAttribute("errorSize", "An error occured!");
        }
        response.sendRedirect("Size");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
