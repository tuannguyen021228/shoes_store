/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import accountDao.CategoryDao;
import accountDao.ShoesDao;
import entities.Account;
import entities.Category;
import entities.Shoes;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
@WebServlet(name = "home", urlPatterns = {"/home"})
public class HomeController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet HomeController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet HomeController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        try {
            ShoesDao shoesDao = new ShoesDao();
            CategoryDao categoryDao = new CategoryDao();
            List<Shoes> shoesList = shoesDao.getAll();
            List<Category> categories = categoryDao.getAll();
            int size = shoesList.size();
            String indexPage = request.getParameter("index");
            int index;
            if (indexPage == null) {
                index = 1;
            }else{
                index = Integer.parseInt(indexPage);
            }
            int endPage = size / 6;
            if(size % 6 != 0){
                endPage++;
            }
            List<Shoes> list = shoesDao.getAllPaging(index);
            session.setAttribute("index", index);
            session.setAttribute("endPage", endPage);
            session.setAttribute("listShoesHome", list);
            session.setAttribute("sizeListShoes", shoesList.size());
            session.setAttribute("categories", categories);
            session.setAttribute("top3highesprice", shoesDao.getTop3HighestPrice());
            session.setAttribute("top3lowetsprice", shoesDao.getTop3LowestPrice());
            session.setAttribute("top3sales", shoesDao.get3Sales());
        } catch (Exception e) {
            e.printStackTrace();
        }
        request.getRequestDispatcher("/home.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        try {
            String shoesNameSearch = request.getParameter("shoesNameSearch");
            ShoesDao shoesDao = new ShoesDao();
            if(shoesNameSearch.equals("") == false){
                session.setAttribute("listShoesHome", shoesDao.getAllSearch(shoesNameSearch));
            }
            session.setAttribute("index", 0);
            request.getRequestDispatcher("/home.jsp").forward(request, response);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
