/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import accountDao.CartDao;
import accountDao.OrderDao;
import accountDao.OrderDetailsDao;
import accountDao.QuantityDao;
import entities.Account;
import entities.Item;
import entities.Order;
import entities.OrderDetails;
import entities.Quantity;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.sql.Date;
import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
@WebServlet(name = "OrderShoes", urlPatterns = {"/OrderShoes"})
public class OrderController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        try {
            int addressId;
            if(request.getParameter("addressId") == null){
                addressId = 0;
            }else{
                addressId = Integer.parseInt(request.getParameter("addressId"));
            }
            OrderDao orderDao = new OrderDao();
            OrderDetailsDao orderDetailsDao = new OrderDetailsDao();
            QuantityDao quantityDao = new QuantityDao();
            CartDao cartDao = new CartDao();
            List<Item> items = (List<Item>) session.getAttribute("items");
            if (items.isEmpty() != true && addressId > 0) {
                double total = (double) session.getAttribute("total");
                Account account = (Account) session.getAttribute("account");
                Order order = new Order(total, account.getAccountId(), addressId, Date.valueOf(LocalDate.now()), Time.valueOf(LocalTime.now()));
                orderDao.create(order);
                List<Order> orders = orderDao.getAllByAcc(account);
                order = orders.get(orders.size()-1);
                for (Item i : items) {
                    OrderDetails orderDetails = new OrderDetails();
                    orderDetails.setOrderId(order.getOrderId());
                    orderDetails.setShoesId(i.getShoes().getShoesId());
                    orderDetails.setQuantity(i.getQuantity());
                    orderDetails.setColorId(i.getColor().getColorId());
                    orderDetails.setSizeId(i.getSize().getSizeId());
                    orderDetails.setUnitPrice(i.getShoes().getPrice() * i.getQuantity());
                    Quantity quantity = quantityDao.getQuantity(i.getShoes().getShoesId(), i.getColor().getColorId(), i.getSize().getSizeId());
                    quantity.setQuantity(quantity.getQuantity() - i.getQuantity());
                    quantityDao.update(quantity);
                    orderDetailsDao.create(orderDetails);
                    cartDao.delete(i.getCartId());
                }
            }else{
                throw new Exception();
            }

        } catch (Exception e) {
            e.printStackTrace();
            request.getRequestDispatcher("cart.jsp").forward(request, response);
        }
        response.sendRedirect("ViewOrder");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
