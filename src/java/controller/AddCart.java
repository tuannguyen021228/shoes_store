/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller;

import accountDao.CartDao;
import accountDao.QuantityDao;
import entities.Account;
import entities.Cart;
import entities.Shoes;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

/**
 *
 * @author TuaniuLinh
 */
@WebServlet(name = "AddCart", urlPatterns = {"/AddCart"})
public class AddCart extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet AddCart</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet AddCart at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("addCartErr", "");
        int quantity = 0;
        int q = 0;
        int colorId = 0;
        int sizeId = 0;
        try {
            CartDao cartDao = new CartDao();
            Account account = (Account) session.getAttribute("account");
            String quan = request.getParameter("quantity");
            if (quan.equals("") == false) {
                quantity = Integer.parseInt(quan);
            }
            Shoes shoes = (Shoes) session.getAttribute("shoesDetail");
            colorId = (int) session.getAttribute("colorId");
            sizeId = (int) session.getAttribute("sizeId");
            QuantityDao quantityDao = new QuantityDao();
            if (colorId > 0 && sizeId > 0) {
                q = quantityDao.getQuantity(shoes.getShoesId(), colorId, sizeId).getQuantity();
            }
            if (colorId > 0 && sizeId > 0 && cartDao.getByAllAttr(account.getAccountId(), shoes.getShoesId(), colorId, sizeId) == null && quantity > 0) {
                if (q > 0) {
                    if (quantity <= q) {
                        Cart cart = new Cart(account.getAccountId(), quantity, shoes.getShoesId(), colorId, sizeId);
                        session.setAttribute("addCartErr", "");
                        cartDao.create(cart);
                    } else {
                        session.setAttribute("addCartErr", "Quantity must less than quantity valid!");
                        request.getRequestDispatcher("shoesDetail?shoesId=shoes.shoesId&colorId=0&sizeId=0").forward(request, response);
                    }
                } else {
                    session.setAttribute("addCartErr", "Quantity valid is 0, see you again <3");
                    request.getRequestDispatcher("shoesDetail?shoesId=shoes.shoesId&colorId=0&sizeId=0").forward(request, response);
                }
            } else if (colorId == 0 && sizeId == 0) {
                session.setAttribute("addCartErr", "Please choose color and size!");
                request.getRequestDispatcher("shoesDetail?shoesId=shoes.shoesId&colorId=0&sizeId=0").forward(request, response);
            } else if (colorId > 0 && sizeId > 0 && cartDao.getByAllAttr(account.getAccountId(), shoes.getShoesId(), colorId, sizeId) != null) {
                session.setAttribute("addCartErr", "Item already exist!");
                request.getRequestDispatcher("shoesDetail?shoesId=shoes.shoesId&colorId=0&sizeId=0").forward(request, response);
            } else if (quantity <= 0) {
                session.setAttribute("addCartErr", "Quantity must larger than 0!");
                request.getRequestDispatcher("shoesDetail?shoesId=shoes.shoesId&colorId=0&sizeId=0").forward(request, response);
            } else {
                session.setAttribute("addCartErr", "Get an error!!!");
                request.getRequestDispatcher("shoesDetail?shoesId=shoes.shoesId&colorId=0&sizeId=0").forward(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        response.sendRedirect("cart");

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
