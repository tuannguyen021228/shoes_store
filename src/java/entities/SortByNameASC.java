/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

import java.util.Comparator;

/**
 *
 * @author TuaniuLinh
 */
public class SortByNameASC implements Comparator<Shoes>{

    @Override
    public int compare(Shoes o1, Shoes o2) {
        return o1.getShoesName().compareTo(o2.getShoesName());
    }
    
}
