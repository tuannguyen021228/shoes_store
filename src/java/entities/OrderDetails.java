/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

/**
 *
 * @author TuaniuLinh
 */
public class OrderDetails {
    private int orderDetailsId;
    private int orderId;
    private int shoesId;
    private int quantity;
    private double unitPrice;
    private Shoes shoes;
    private int colorId;
    private int sizeId;

    public OrderDetails() {
    }

    public OrderDetails(int orderDetailsId, int orderId, int shoesId, int quantity, double unitPrice) {
        this.orderDetailsId = orderDetailsId;
        this.orderId = orderId;
        this.shoesId = shoesId;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }
    
    public OrderDetails(int orderId, int shoesId, int quantity, double unitPrice) {
        this.orderId = orderId;
        this.shoesId = shoesId;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
    }

    public int getOrderDetailsId() {
        return orderDetailsId;
    }

    public void setOrderDetailsId(int orderDetailsId) {
        this.orderDetailsId = orderDetailsId;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public int getShoesId() {
        return shoesId;
    }

    public void setShoesId(int shoesId) {
        this.shoesId = shoesId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Shoes getShoes() {
        return shoes;
    }

    public void setShoes(Shoes shoes) {
        this.shoes = shoes;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }
    
    
    
}
