/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

/**
 *
 * @author TuaniuLinh
 */
public class Cart {
    private int cartId;
    private int accountId;
    private int quantity;
    private int shoesId;
    private int colorId;
    private int sizeId;

    public Cart() {
    }

    public Cart(int cartId, int accountId, int quantity, int shoesId, int colorId, int sizeId) {
        this.cartId = cartId;
        this.accountId = accountId;
        this.quantity = quantity;
        this.shoesId = shoesId;
        this.colorId = colorId;
        this.sizeId = sizeId;
    }
    
    public Cart(int accountId, int quantity, int shoesId, int colorId, int sizeId) {
        this.accountId = accountId;
        this.quantity = quantity;
        this.shoesId = shoesId;
        this.colorId = colorId;
        this.sizeId = sizeId;
    }

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getShoesId() {
        return shoesId;
    }

    public void setShoesId(int shoesId) {
        this.shoesId = shoesId;
    }

    public int getColorId() {
        return colorId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public int getSizeId() {
        return sizeId;
    }

    public void setSizeId(int sizeId) {
        this.sizeId = sizeId;
    }
    
    
}
