/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entities;

import java.sql.Date;
import java.sql.Time;

/**
 *
 * @author TuaniuLinh
 */
public class Order {
    private int orderId;
    private double totalPrice;
    private int accountId;
    private int addressId;
    private Date orderDate;
    private Time orderTime;
    private AddressShip addressShip;

    public Order() {
    }

    public Order(int orderId, double totalPrice, int accountId, int addressId, Date orderDate, Time orderTime) {
        this.orderId = orderId;
        this.totalPrice = totalPrice;
        this.accountId = accountId;
        this.addressId = addressId;
        this.orderDate = orderDate;
        this.orderTime = orderTime;
    }
    
    public Order(double totalPrice, int accountId, int addressId, Date orderDate, Time orderTime) {
        this.totalPrice = totalPrice;
        this.accountId = accountId;
        this.addressId = addressId;
        this.orderDate = orderDate;
        this.orderTime = orderTime;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public int getAccountId() {
        return accountId;
    }

    public void setAccountId(int accountId) {
        this.accountId = accountId;
    }

    public int getAddressId() {
        return addressId;
    }

    public void setAddressId(int addressId) {
        this.addressId = addressId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public Time getOrderTime() {
        return orderTime;
    }

    public void setOrderTime(Time orderTime) {
        this.orderTime = orderTime;
    }

    public AddressShip getAddressShip() {
        return addressShip;
    }

    public void setAddressShip(AddressShip addressShip) {
        this.addressShip = addressShip;
    }
    
}
