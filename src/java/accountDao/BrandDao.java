/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Brand;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class BrandDao {
    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;
    
    public void create(Brand brand) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO brand (brand_name) VALUES (?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, brand.getBrandName());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public List<Brand> getAll() throws SQLException {
        List<Brand> list = new ArrayList<Brand>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM brand";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Brand brand = new Brand();
                brand.setBrandId(rs.getInt(1));
                brand.setBrandName(rs.getString(2));
                list.add(brand);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }
    
    public void delete(int id) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "DELETE FROM brand WHERE brand_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public Brand getBrandById(int id) throws SQLException {
        Brand brand = new Brand();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM brand where brand_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                brand.setBrandId(rs.getInt(1));
                brand.setBrandName(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return brand;
    }
}
