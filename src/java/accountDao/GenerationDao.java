/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Generation;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class GenerationDao {
    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;
    
    public List<Generation> getAll() throws SQLException {
        List<Generation> list = new ArrayList<Generation>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM generation";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Generation gen = new Generation();
                gen.setGenerationId(rs.getInt(1));
                gen.setGenerationName(rs.getString(2));
                list.add(gen);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }
    
    public Generation getCategoryById(int id) throws SQLException {
        Generation gen = new Generation();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM generation where generation_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                gen.setGenerationId(rs.getInt(1));
                gen.setGenerationName(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return gen;
    }
}
