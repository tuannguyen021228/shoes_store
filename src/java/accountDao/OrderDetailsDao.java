/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.OrderDetails;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author TuaniuLinh
 */
public class OrderDetailsDao {
    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;
    
    public void create(OrderDetails orderDetails) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO order_details (order_id, shoes_id, quantity, unit_price, color_id, size_id) VALUES (?,?,?,?,?,?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, orderDetails.getOrderId());
            pstmt.setInt(2, orderDetails.getShoesId());
            pstmt.setInt(3, orderDetails.getQuantity());
            pstmt.setDouble(4, orderDetails.getUnitPrice());
            pstmt.setDouble(5, orderDetails.getColorId());
            pstmt.setDouble(6, orderDetails.getSizeId());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public List<OrderDetails> getAll(int orderId) throws SQLException {
        List<OrderDetails> list = new ArrayList<OrderDetails>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM order_details where order_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, orderId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                OrderDetails details = new OrderDetails();
                details.setOrderDetailsId(rs.getInt(1));
                details.setOrderId(rs.getInt(2));
                details.setShoesId(rs.getInt(3));
                details.setQuantity(rs.getInt(4));
                details.setUnitPrice(rs.getDouble(5));
                details.setColorId(rs.getInt(7));
                details.setSizeId(rs.getInt(8));
                list.add(details);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }
    
    public List<OrderDetails> getAll() throws SQLException {
        List<OrderDetails> list = new ArrayList<OrderDetails>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM order_details";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                OrderDetails details = new OrderDetails();
                details.setOrderDetailsId(rs.getInt(1));
                details.setOrderId(rs.getInt(2));
                details.setShoesId(rs.getInt(3));
                details.setQuantity(rs.getInt(4));
                details.setUnitPrice(rs.getDouble(5));
                list.add(details);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }
    
    public void delete(int orderId) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "DELETE FROM order_details WHERE order_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, orderId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
}
