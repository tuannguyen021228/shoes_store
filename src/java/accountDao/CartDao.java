/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Cart;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class CartDao {

    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;

    public void create(Cart cart) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO cart (account_id, quantity, shoes_id, color_id, size_id) VALUES (?, ?, ?, ?, ?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, cart.getAccountId());
            pstmt.setInt(2, cart.getQuantity());
            pstmt.setInt(3, cart.getShoesId());
            pstmt.setInt(4, cart.getColorId());
            pstmt.setInt(5, cart.getSizeId());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }

    public List<Cart> getAll() throws SQLException {
        List<Cart> list = new ArrayList<Cart>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM cart";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Cart cart = new Cart();
                cart.setCartId(rs.getInt(1));
                cart.setAccountId(rs.getInt(2));
                cart.setQuantity(rs.getInt(3));
                cart.setShoesId(rs.getInt(4));
                cart.setColorId(rs.getInt(5));
                cart.setSizeId(rs.getInt(6));
                list.add(cart);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public Cart getByAllAttr(int accountId, int shoesId, int colorId, int sizeId) throws SQLException {
        Cart cart = new Cart();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM cart WHERE account_id = ? and shoes_id = ? and color_id = ? and size_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, accountId);
            pstmt.setInt(2, shoesId);
            pstmt.setInt(3, colorId);
            pstmt.setInt(4, sizeId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                cart.setCartId(rs.getInt(1));
                cart.setAccountId(rs.getInt(2));
                cart.setQuantity(rs.getInt(3));
                cart.setShoesId(rs.getInt(4));
                cart.setColorId(rs.getInt(5));
                cart.setSizeId(rs.getInt(6));
                return cart;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return null;
    }

    public void delete(int id) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "DELETE FROM cart WHERE cart_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
}
