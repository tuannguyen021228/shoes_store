/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Account;
import entities.Order;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class OrderDao {
    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;
    
    public void create(Order order) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO orders (total_price, order_date, order_time, account_id, address_id) VALUES (?,?,?,?,?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setDouble(1, order.getTotalPrice());
            pstmt.setDate(2, order.getOrderDate());
            pstmt.setTime(3, order.getOrderTime());
            pstmt.setInt(4, order.getAccountId());
            pstmt.setInt(5, order.getAddressId());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public List<Order> getAllByAcc(Account account) throws SQLException {
        List<Order> orders = new ArrayList<Order>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM orders where account_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, account.getAccountId());
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Order order = new Order();
                order.setOrderId(rs.getInt(1));
                order.setTotalPrice(rs.getDouble(2));
                order.setOrderDate(rs.getDate(3));
                order.setOrderTime(rs.getTime(4));
                order.setAccountId(rs.getInt(5));
                order.setAddressId(rs.getInt(6));
                orders.add(order);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return orders;
    }
    
    public void delete(int orderId) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "DELETE FROM orders WHERE order_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, orderId);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
}
