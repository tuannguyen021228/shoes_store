/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Account;
import entities.AddressShip;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class AddressShipDao {
    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;

    public void create(AddressShip addressShip) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO addressShip (account_id, address_name) VALUES (?, ?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, addressShip.getAccountId());
            pstmt.setString(2, addressShip.getAddressName());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public List<AddressShip> getListAddressShip(Account a) throws SQLException {
        List<AddressShip> list = new ArrayList<AddressShip>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM addressShip WHERE account_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, a.getAccountId());
            rs = pstmt.executeQuery();
            while (rs.next()) {
                AddressShip addressShip = new AddressShip();
                addressShip.setAddressId(rs.getInt(1));
                addressShip.setAccountId(rs.getInt(2));
                addressShip.setAddressName(rs.getString(3));
                list.add(addressShip);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }
    
    public void delete(int id) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "DELETE FROM addressShip WHERE address_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public AddressShip getAddressShipById(int id) throws SQLException {
        AddressShip addressShip = new AddressShip();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM addressShip WHERE address_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                addressShip.setAddressId(rs.getInt(1));
                addressShip.setAccountId(rs.getInt(2));
                addressShip.setAddressName(rs.getString(3));
                return addressShip;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return null;
    }
}
