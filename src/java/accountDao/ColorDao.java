/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Color;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class ColorDao {
    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;
    
    public void create(Color color) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO color (color_name) VALUES (?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, color.getColorName());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public List<Color> getAll() throws SQLException {
        List<Color> list = new ArrayList<Color>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM color";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Color color = new Color();
                color.setColorId(rs.getInt(1));
                color.setColorName(rs.getString(2));
                list.add(color);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }
    
    public void delete(int id) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "DELETE FROM color WHERE color_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public Color getByColorId(int id) throws SQLException {
        Color color = new Color();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM color where color_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                color.setColorId(rs.getInt(1));
                color.setColorName(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return color;
    }
}
