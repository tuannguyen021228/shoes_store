/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Category;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class CategoryDao {

    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;

    public void create(Category category) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO category (category_name) VALUES (?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, category.getCategoryName());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }

    public List<Category> getAll() throws SQLException {
        List<Category> list = new ArrayList<Category>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM category";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Category category = new Category();
                category.setCategoryId(rs.getInt(1));
                category.setCategoryName(rs.getString(2));
                list.add(category);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public void delete(int id) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "DELETE FROM category WHERE category_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }

    public Category getCategoryById(int id) throws SQLException {
        Category category = new Category();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM category where category_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                category.setCategoryId(rs.getInt(1));
                category.setCategoryName(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return category;
    }
}
