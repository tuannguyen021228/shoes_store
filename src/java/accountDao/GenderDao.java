/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Gender;
import entities.ShoesGender;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class GenderDao {
    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;
    
    public List<Gender> getAll() throws SQLException {
        List<Gender> list = new ArrayList<Gender>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM gender";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Gender gender = new Gender();
                gender.setGenderId(rs.getInt(1));
                gender.setGenderName(rs.getString(2));
                list.add(gender);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }
    
    public Gender getGenderByShoesId(ShoesGender sg) throws SQLException {
        Gender gen = new Gender();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM gender where gender_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, sg.getGenderId());
            rs = pstmt.executeQuery();
            while (rs.next()) {
                gen.setGenderId(rs.getInt(1));
                gen.setGenderName(rs.getString(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return gen;
    }
}
