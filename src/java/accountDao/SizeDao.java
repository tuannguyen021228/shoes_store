/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Size;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class SizeDao {
    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;
    
    public void create(Size size) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO size (sizes) VALUES (?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setDouble(1, size.getSizes());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public List<Size> getAll() throws SQLException {
        List<Size> list = new ArrayList<Size>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM size";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Size size = new Size();
                size.setSizeId(rs.getInt(1));
                size.setSizes(rs.getDouble(2));
                list.add(size);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }
    
    public void delete(int id) throws SQLException{
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "DELETE FROM size WHERE size_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public Size getBySizeId(int id) throws SQLException {
        Size size = new Size();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM size where size_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                size.setSizeId(rs.getInt(1));
                size.setSizes(rs.getDouble(2));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return size;
    }
}
