/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Account;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class AccountDao {

    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;

    public void register(Account account) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO account (user_name, password, register_date, is_update_information, role) VALUES (?, ?, ?, ?, ?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, account.getUserName());
            pstmt.setString(2, account.getPassword());
            pstmt.setDate(3, Date.valueOf(LocalDate.now()));
            pstmt.setBoolean(4, false);
            pstmt.setString(5, "member");
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }

    public Account login(String userName, String password) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM account WHERE user_name = ? and password = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, userName);
            pstmt.setString(2, password);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setAccountId(rs.getInt(1));
                account.setEmail(rs.getString(2));
                account.setUserName(rs.getString(3));
                account.setPassword(rs.getString(4));
                account.setAddress(rs.getString(5));
                account.setFullName(rs.getString(6));
                account.setPhoneNumber(rs.getString(7));
                account.setRole(rs.getString(8));
                account.setGender(rs.getString(9));
                account.setRegisterDate(rs.getDate(10));
                account.setIsUpdateInformation(rs.getBoolean(11));
                return account;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return null;
    }

    public void updateInfo(String userName, Account a) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "update account set email = ?, full_name = ?, address = ?, gender = ?, phone_number = ?, is_update_information = ? where user_name = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, a.getEmail());
            pstmt.setString(2, a.getFullName());
            pstmt.setString(3, a.getAddress());
            pstmt.setString(4, a.getGender());
            pstmt.setString(5, a.getPhoneNumber());
            pstmt.setBoolean(6, true);
            pstmt.setString(7, userName);
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }

    public Account getAcc(String userName) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM account WHERE user_name = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, userName);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setAccountId(rs.getInt(1));
                account.setEmail(rs.getString(2));
                account.setUserName(rs.getString(3));
                account.setPassword(rs.getString(4));
                account.setAddress(rs.getString(5));
                account.setFullName(rs.getString(6));
                account.setPhoneNumber(rs.getString(7));
                account.setRole(rs.getString(8));
                account.setGender(rs.getString(9));
                account.setRegisterDate(rs.getDate(10));
                account.setIsUpdateInformation(rs.getBoolean(11));
                return account;

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return null;
    }

    public List<Account> getAll() throws SQLException {
        List<Account> listAcc = new ArrayList<Account>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM account";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setAccountId(rs.getInt(1));
                account.setEmail(rs.getString(2));
                account.setUserName(rs.getString(3));
                account.setPassword(rs.getString(4));
                account.setAddress(rs.getString(5));
                account.setFullName(rs.getString(6));
                account.setPhoneNumber(rs.getString(7));
                account.setRole(rs.getString(8));
                account.setGender(rs.getString(9));
                account.setRegisterDate(rs.getDate(10));
                account.setIsUpdateInformation(rs.getBoolean(11));
                if (account.getUserName().equals("admin") == false) {
                    listAcc.add(account);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return listAcc;
    }

    public List<Account> getAllByDateFromAndTo(Date dateFrom, Date dateTo) throws SQLException {
        List<Account> listAcc = new ArrayList<Account>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM account where register_date >= ? and register_date <= ?";
            pstmt = con.prepareStatement(sql);
//            pstmt.setString(1, "'" + dateFrom + "'");
//            pstmt.setString(2, "'" + dateTo + "'");
            pstmt.setString(1, "'" + dateFrom + "'");
            pstmt.setString(2, "'" + dateTo + "'");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Account account = new Account();
                account.setAccountId(rs.getInt(1));
                account.setEmail(rs.getString(2));
                account.setUserName(rs.getString(3));
                account.setPassword(rs.getString(4));
                account.setAddress(rs.getString(5));
                account.setFullName(rs.getString(6));
                account.setPhoneNumber(rs.getString(7));
                account.setRole(rs.getString(8));
                account.setGender(rs.getString(9));
                account.setRegisterDate(rs.getDate(10));
                account.setIsUpdateInformation(rs.getBoolean(11));
                if (account.getUserName().equals("admin") == false) {
                    listAcc.add(account);
                }

            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return listAcc;
    }

}
