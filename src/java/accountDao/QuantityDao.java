/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Quantity;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class QuantityDao {

    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;

    public void create(Quantity quantity) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO quantity (shoes_id, size_id, color_id, quantity) VALUES (?, ?, ?, ?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, quantity.getShoesId());
            pstmt.setInt(2, quantity.getSizeId());
            pstmt.setInt(3, quantity.getColorId());
            pstmt.setInt(4, quantity.getQuantity());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
    
    public List<Quantity> getQuantity(int shoesId) throws SQLException {
        List<Quantity> list = new ArrayList<Quantity>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM quantity where shoes_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, shoesId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Quantity quantity = new Quantity();
                quantity.setShoesId(rs.getInt(1));
                quantity.setSizeId(rs.getInt(2));
                quantity.setColorId(rs.getInt(3));
                quantity.setQuantity(rs.getInt(4));
                list.add(quantity);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }
    
    public Quantity getQuantity(int shoesId, int colorId, int sizeId) throws SQLException {
        Quantity quantity = new Quantity();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM quantity where shoes_id = ? and color_id = ? and size_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, shoesId);
            pstmt.setInt(2, colorId);
            pstmt.setInt(3, sizeId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                quantity.setShoesId(rs.getInt(1));
                quantity.setSizeId(rs.getInt(2));
                quantity.setColorId(rs.getInt(3));
                quantity.setQuantity(rs.getInt(4));
                return quantity;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void update(Quantity quantity) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "update quantity set quantity = ? where shoes_id = ? and color_id = ? and size_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, quantity.getQuantity());
            pstmt.setInt(2, quantity.getShoesId());
            pstmt.setInt(3, quantity.getColorId());
            pstmt.setInt(4, quantity.getSizeId());
            pstmt.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }
}
