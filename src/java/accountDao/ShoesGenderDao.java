/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Generation;
import entities.ShoesGender;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class ShoesGenderDao {

    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;

    public void create(ShoesGender sg) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO shoes_gender (shoes_id, gender_id) VALUES (?, ?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, sg.getShoesId());
            pstmt.setInt(2, sg.getGenderId());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }

    public List<ShoesGender> getGenderByShoesId(int id) throws SQLException {
        List<ShoesGender> list = new ArrayList<ShoesGender>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM shoes_gender where shoes_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                ShoesGender gen = new ShoesGender();
                gen.setShoesId(rs.getInt(1));
                gen.setGenderId(rs.getInt(2));
                list.add(gen);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }
}
