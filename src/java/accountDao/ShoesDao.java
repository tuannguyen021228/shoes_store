/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package accountDao;

import common.DBUtils;
import entities.Category;
import entities.Color;
import entities.Shoes;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TuaniuLinh
 */
public class ShoesDao {

    private Connection con = null;
    private PreparedStatement pstmt = null;
    private String sql = null;
    private ResultSet rs = null;

    public List<Shoes> getAll() throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM shoes";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> get3Sales() throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "select s.shoes_id, s.category_id, s.brand_id, s.generation_id, s.shoes_name, s.price, s.description, s.image\n"
                    + "from shoes s\n"
                    + "inner join order_details od\n"
                    + "on s.shoes_id = od.shoes_id\n"
                    + "where od.order_details_id IN (\n"
                    + "select TOP(3) order_details_id from order_details\n"
                    + "group by shoes_id, order_details_id\n"
                    + "order by COUNT(shoes_id) desc\n"
                    + ")";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getTop3HighestPrice() throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "  select TOP(3) * from shoes \n"
                    + "  order by price desc";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getTop3LowestPrice() throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "  select TOP(3) * from shoes \n"
                    + "  order by price asc";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getAllByCategoryId(int categoryId) throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM shoes where category_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, categoryId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getAllByGender(int genderId) throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "  SELECT * FROM shoes s\n"
                    + "  inner join shoes_gender g \n"
                    + "  on s.shoes_id = g.shoes_id\n"
                    + "  where g.gender_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, genderId);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getAllByPrice(int from, int to) throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            if (to == -1) {
                sql = "SELECT * FROM shoes WHERE price >= ?";
            } else {
                sql = "SELECT * FROM shoes WHERE price >= ? and price < ?";
            }
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, from);
            if (to != -1) {
                pstmt.setInt(2, to);
            }
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getShoesUnisex() throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "select s.shoes_id\n"
                    + "      ,s.category_id\n"
                    + "      ,s.brand_id\n"
                    + "      ,s.generation_id\n"
                    + "      ,s.shoes_name\n"
                    + "      ,s.price\n"
                    + "      ,s.description\n"
                    + "      ,s.image\n"
                    + "	  ,COUNT(g.gender_id)\n"
                    + "  From shoes s\n"
                    + "  inner join shoes_gender g\n"
                    + "  on s.shoes_id = g.shoes_id\n"
                    + "  group by s.shoes_id\n"
                    + "      ,s.category_id\n"
                    + "      ,s.brand_id\n"
                    + "      ,s.generation_id\n"
                    + "      ,s.shoes_name\n"
                    + "      ,s.price\n"
                    + "      ,s.description\n"
                    + "      ,s.image\n"
                    + "  having count(g.gender_id) = 2";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getAllSortByNameASC() throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "  SELECT * FROM shoes\n"
                    + "  ORDER BY shoes_name ASC";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getAllSortByNameDESC() throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "  SELECT * FROM shoes\n"
                    + "  ORDER BY shoes_name DESC";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getAllSortByPriceASC() throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "  SELECT * FROM shoes\n"
                    + "  ORDER BY price ASC";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getAllSortByPriceDESC() throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "  SELECT * FROM shoes\n"
                    + "  ORDER BY price DESC";
            pstmt = con.prepareStatement(sql);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public List<Shoes> getAllSearch(String nameSearch) throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM shoes Where shoes_name LIKE ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, "%" + nameSearch + "%");
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public Shoes getShoesByName(String shoesName) throws SQLException {
        Shoes shoes = new Shoes();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM shoes where shoes_name = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setString(1, shoesName);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return shoes;
    }

    public List<Shoes> getAllPaging(int index) throws SQLException {
        List<Shoes> list = new ArrayList<Shoes>();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = " SELECT * FROM shoes\n"
                    + " ORDER BY shoes_id\n"
                    + " OFFSET ? ROWS FETCH NEXT 6 ROWS ONLY";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, (index - 1) * 6);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                Shoes shoes = new Shoes();
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                list.add(shoes);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return list;
    }

    public Shoes getShoesById(int id) throws SQLException {
        Shoes shoes = new Shoes();
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "SELECT * FROM shoes where shoes_id = ?";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, id);
            rs = pstmt.executeQuery();
            while (rs.next()) {
                shoes.setShoesId(rs.getInt(1));
                shoes.setCategoryId(rs.getInt(2));
                shoes.setBrandId(rs.getInt(3));
                shoes.setGenerationId(rs.getInt(4));
                shoes.setShoesName(rs.getString(5));
                shoes.setPrice(rs.getDouble(6));
                shoes.setDescription(rs.getString(7));
                shoes.setImage(rs.getString(8));
                try {
                    CategoryDao categoryDao = new CategoryDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    BrandDao brandDao = new BrandDao();
                    GenerationDao generationDao = new GenerationDao();
                    shoes.setCategory(categoryDao.getCategoryById(shoes.getCategoryId()));
                    shoes.setBrand(brandDao.getBrandById(shoes.getBrandId()));
                    shoes.setGeneration(generationDao.getCategoryById(shoes.getGenerationId()));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(rs, pstmt, con);
        }
        return shoes;
    }

    public void create(Shoes shoes) throws SQLException {
        try {
            con = DBUtils.getInstance().getConnection();
            sql = "INSERT INTO shoes (category_id, brand_id, generation_id, shoes_name, price, description, image) VALUES (?, ?, ?, ?, ?, ?, ?)";
            pstmt = con.prepareStatement(sql);
            pstmt.setInt(1, shoes.getCategoryId());
            pstmt.setInt(2, shoes.getBrandId());
            pstmt.setInt(3, shoes.getGenerationId());
            pstmt.setString(4, shoes.getShoesName());
            pstmt.setDouble(5, shoes.getPrice());
            pstmt.setString(6, shoes.getDescription());
            pstmt.setString(7, shoes.getImage());
            pstmt.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DBUtils.ReleaseJDBC(null, pstmt, con);
        }
    }

}
