<%-- 
    Document   : shoesDetail
    Created on : Oct 24, 2022, 5:38:11 PM
    Author     : TuaniuLinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- VALIDATION PLUGIN -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <!-- ICON -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
        <link rel="stylesheet" href="css/all.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <script src="js/quantityIncreAndDecre.js"></script>
        <title>Art shoes store</title>
    </head>
    <body>
        <style>
            #nghe{
                background-image: url('img/ngheee.jpg');
                background-repeat: no-repeat;
                background-size: cover;
            }
            .error {
                color: red;
                text-align: center;
            }
        </style>
        <jsp:include page="header.jsp"/>
        <c:choose>
            <c:when test="${sessionScope.account != null}">
                <c:choose>
                    <c:when test="${sessionScope.account.role eq 'member'}">
                        <form action="AddCart" method="get">
                            <div id="nghe">
                                <div class="py-5 d-flex justify-content-center align-items-center">
                                    <div class="row bg-dark my-5 mx-5 bg-body" style="height: 565px; width: 900px;border: 5px outset rgba(164,68,162,0.62);
                                         border-radius: 40px;" style="margin: 0; padding: 0">
                                        <div class="col-6">
                                            <img class="mt-3" style="width: 100%;max-width: 500px;height: auto;border: 0px outset transparent;
                                                 border-radius: 40px;" src="${sessionScope.shoesDetail.image}" alt="">
                                        </div>
                                        <div class="col-6">
                                            <div style="height: 450px">
                                                <h3 class="fw-bold mt-4 ms-3">${sessionScope.shoesDetail.shoesName}</h3>
                                                <p class="ms-3 mt-3">Category: ${sessionScope.shoesDetail.category.categoryName}</p>
                                                <p class="ms-3 mt-3">Brand: ${sessionScope.shoesDetail.brand.brandName}</p>
                                                <p class="ms-3 mt-3">Collections: <c:forEach items="${sessionScope.genders}" var="a">${a.genderName}, </c:forEach>${sessionScope.shoesDetail.generation.generationName}</p>
                                                    <div class="mt-3 row" style="margin-left: 5px">
                                                        <label class="col-2">Colors: </label>
                                                        <div class="col-10 row">
                                                        <c:forEach items="${sessionScope.colors}" var="a" varStatus="count">
                                                            <div style="border: 2px solid <c:choose><c:when test="${sessionScope.colorId == a.colorId}">#000000</c:when><c:otherwise>#ACDFFF</c:otherwise></c:choose>;
                                                                 border-radius: 15px;" class="col-4 text-center">
                                                                     <a class="text-decoration-none text-dark" href="shoesDetail?shoesId=${sessionScope.shoesDetail.shoesId}&colorId=${a.colorId}&sizeId=${sessionScope.sizeId}">
                                                                    <div>
                                                                        ${a.colorName}
                                                                    </div>
                                                                </a>
                                                            </div>

                                                        </c:forEach>
                                                    </div>
                                                </div>
                                                <div class="mt-3 row" style="margin-left: 5px">
                                                    <label class="col-2">Sizes: </label>
                                                    <div class="col-10 row">
                                                        <c:forEach items="${sessionScope.sizes}" var="a" varStatus="count">
                                                            <div style="border: 2px solid <c:choose><c:when test="${sessionScope.sizeId == a.sizeId}">#000000</c:when><c:otherwise>#ACDFFF</c:otherwise></c:choose>;
                                                                 border-radius: 15px;" class="col-3 text-center">
                                                                     <a class="text-decoration-none text-dark" href="shoesDetail?shoesId=${sessionScope.shoesDetail.shoesId}&colorId=${sessionScope.colorId}&sizeId=${a.sizeId}">
                                                                    <div>
                                                                        ${a.sizes}
                                                                    </div>
                                                                </a>
                                                            </div>
                                                        </c:forEach>
                                                    </div>
                                                </div>
                                                <div class="row mt-3">
                                                    <div class="col-5">
                                                        <p class="ms-3 mt-3">Quantity: ${sessionScope.quan} (Valid)</p>
                                                    </div>
                                                    <div class="col-7">

                                                        <div class="btn-group mt-2 float-end me-3 row">
                                                            <div class="col-4 mt-2">
                                                                <label for="quantity" class="form-label">Choose: </label>
                                                            </div>
                                                            <div class="col-8">
                                                                <input type="number" class="form-control border-dark" id="quantity" name="quantity">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <c:set var="quan" value="quantity"></c:set>
                                                        <input hidden id="quan" name="quan" value="quan">
                                                    </div>
                                                    <h4 class="fw-bold mt-3 ms-3">Price: $${sessionScope.shoesDetail.price}</h4>
                                                <c:choose>
                                                    <c:when test="${sessionScope.addCartErr != ''}">
                                                        <div class="error mt-4">
                                                            ${sessionScope.addCartErr}
                                                        </div>
                                                    </c:when>
                                                </c:choose>
                                            </div>
                                            <div class="text-center" style="width: 100%">
                                                <button type="submit" class="btn btn-lg btn-dark"><i class="fa-solid fa-cart-shopping"></i> Add to cart</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </c:when>
                    <c:otherwise>
                        <div id="nghe">
                            <div class="py-5 d-flex justify-content-center align-items-center">
                                <div class="row bg-dark my-5 mx-5 bg-body" style="height: 565px; width: 900px;border: 5px outset rgba(164,68,162,0.62);
                                     border-radius: 40px;" style="margin: 0; padding: 0">
                                    <div class="col-6">
                                        <img class="mt-3" style="width: 100%;max-width: 500px;height: auto;border: 0px outset transparent;
                                             border-radius: 40px;" src="${sessionScope.shoesDetail.image}" alt="">
                                    </div>
                                    <div class="col-6">
                                        <div style="height: 450px">
                                            <h3 class="fw-bold mt-4 ms-3">${sessionScope.shoesDetail.shoesName}</h3>
                                            <p class="ms-3 mt-3">Category: ${sessionScope.shoesDetail.category.categoryName}</p>
                                            <p class="ms-3 mt-3">Brand: ${sessionScope.shoesDetail.brand.brandName}</p>
                                            <p class="ms-3 mt-3">Collections: <c:forEach items="${sessionScope.genders}" var="a">${a.genderName}, </c:forEach>${sessionScope.shoesDetail.generation.generationName}</p>
                                                <div class="mt-3 row" style="margin-left: 5px">
                                                    <label class="col-2">Colors: </label>
                                                    <div class="col-10 row">
                                                    <c:forEach items="${sessionScope.colors}" var="a" varStatus="count">
                                                        <div style="border: 2px solid <c:choose><c:when test="${sessionScope.colorId == a.colorId}">#000000</c:when><c:otherwise>#ACDFFF</c:otherwise></c:choose>;
                                                             border-radius: 15px;" class="col-4 text-center">
                                                                 <a class="text-decoration-none text-dark" href="shoesDetail?shoesId=${sessionScope.shoesDetail.shoesId}&colorId=${a.colorId}&sizeId=${sessionScope.sizeId}">
                                                                <div>
                                                                    ${a.colorName}
                                                                </div>
                                                            </a>
                                                        </div>

                                                    </c:forEach>
                                                </div>
                                            </div>
                                            <div class="mt-3 row" style="margin-left: 5px">
                                                <label class="col-2">Sizes: </label>
                                                <div class="col-10 row">
                                                    <c:forEach items="${sessionScope.sizes}" var="a" varStatus="count">
                                                        <div style="border: 2px solid <c:choose><c:when test="${sessionScope.sizeId == a.sizeId}">#000000</c:when><c:otherwise>#ACDFFF</c:otherwise></c:choose>;
                                                             border-radius: 15px;" class="col-3 text-center">
                                                                 <a class="text-decoration-none text-dark" href="shoesDetail?shoesId=${sessionScope.shoesDetail.shoesId}&colorId=${sessionScope.colorId}&sizeId=${a.sizeId}">
                                                                <div>
                                                                    ${a.sizes}
                                                                </div>
                                                            </a>
                                                        </div>
                                                    </c:forEach>
                                                </div>
                                            </div>
                                            <div class="row mt-3">
                                                <div class="col-5">
                                                    <p class="ms-3 mt-3">Quantity: ${sessionScope.quan} (Valid)</p>
                                                </div>
                                                <div class="col-7">

                                                    <div class="btn-group mt-2 float-end me-3 row">
                                                        <div class="col-4 mt-2">
                                                            <label for="quantity" class="form-label">Choose: </label>
                                                        </div>
                                                        <div class="col-8">
                                                            <input type="number" class="form-control border-dark" id="quantity" name="quantity">
                                                        </div>
                                                    </div>
                                                </div>
                                                <c:set var="quan" value="quantity"></c:set>
                                                    <input hidden id="quan" name="quan" value="quan">
                                                </div>
                                                <h4 class="fw-bold mt-3 ms-3">Price: $${sessionScope.shoesDetail.price}</h4>
                                        </div>
                                        <div class="text-center" style="width: 100%">
                                            <button class="btn btn-lg btn-dark">Welcome admin <3</button>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </c:when>
            <c:otherwise>
                <div id="nghe">
                    <div class="py-5 d-flex justify-content-center align-items-center">
                        <div class="row bg-dark my-5 mx-5 bg-body" style="height: 565px; width: 900px;border: 5px outset rgba(164,68,162,0.62);
                             border-radius: 40px;" style="margin: 0; padding: 0">
                            <div class="col-6">
                                <img class="mt-3" style="width: 100%;max-width: 500px;height: auto;border: 0px outset transparent;
                                     border-radius: 40px;" src="${sessionScope.shoesDetail.image}" alt="">
                            </div>
                            <div class="col-6">
                                <div style="height: 450px">
                                    <h3 class="fw-bold mt-4 ms-3">${sessionScope.shoesDetail.shoesName}</h3>
                                    <p class="ms-3 mt-3">Category: ${sessionScope.shoesDetail.category.categoryName}</p>
                                    <p class="ms-3 mt-3">Brand: ${sessionScope.shoesDetail.brand.brandName}</p>
                                    <p class="ms-3 mt-3">Collections: <c:forEach items="${sessionScope.genders}" var="a">${a.genderName}, </c:forEach>${sessionScope.shoesDetail.generation.generationName}</p>
                                        <div class="mt-3 row" style="margin-left: 5px">
                                            <label class="col-2">Colors: </label>
                                            <div class="col-10 row">
                                            <c:forEach items="${sessionScope.colors}" var="a" varStatus="count">
                                                <div style="border: 2px solid <c:choose><c:when test="${sessionScope.colorId == a.colorId}">#000000</c:when><c:otherwise>#ACDFFF</c:otherwise></c:choose>;
                                                     border-radius: 15px;" class="col-4 text-center">
                                                         <a class="text-decoration-none text-dark" href="shoesDetail?shoesId=${sessionScope.shoesDetail.shoesId}&colorId=${a.colorId}&sizeId=${sessionScope.sizeId}">
                                                        <div>
                                                            ${a.colorName}
                                                        </div>
                                                    </a>
                                                </div>

                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="mt-3 row" style="margin-left: 5px">
                                        <label class="col-2">Sizes: </label>
                                        <div class="col-10 row">
                                            <c:forEach items="${sessionScope.sizes}" var="a" varStatus="count">
                                                <div style="border: 2px solid <c:choose><c:when test="${sessionScope.sizeId == a.sizeId}">#000000</c:when><c:otherwise>#ACDFFF</c:otherwise></c:choose>;
                                                     border-radius: 15px;" class="col-3 text-center">
                                                         <a class="text-decoration-none text-dark" href="shoesDetail?shoesId=${sessionScope.shoesDetail.shoesId}&colorId=${sessionScope.colorId}&sizeId=${a.sizeId}">
                                                        <div>
                                                            ${a.sizes}
                                                        </div>
                                                    </a>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-5">
                                            <p class="ms-3 mt-3">Quantity: ${sessionScope.quan} (Valid)</p>
                                        </div>
                                        <div class="col-7">

                                            <div class="btn-group mt-2 float-end me-3 row">
                                                <div class="col-4 mt-2">
                                                    <label for="quantity" class="form-label">Choose: </label>
                                                </div>
                                                <div class="col-8">
                                                    <input type="number" class="form-control border-dark" id="quantity" name="quantity">
                                                </div>
                                            </div>
                                        </div>
                                        <c:set var="quan" value="quantity"></c:set>
                                            <input hidden id="quan" name="quan" value="quan">
                                        </div>
                                        <h4 class="fw-bold mt-3 ms-3">Price: $${sessionScope.shoesDetail.price}</h4>
                                </div>
                                <div class="text-center" style="width: 100%">
                                    <a href="login">
                                        <button class="btn btn-lg btn-dark"><i class="fa-solid fa-right-to-bracket"></i> Login To Buy</button>
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </c:otherwise>
        </c:choose>
        <div class="my-5 mx-5 px-5"> 
            <h2>Description</h2>
            <h5 class="lh-base">${sessionScope.shoesDetail.description}</h5>
        </div>
    </body>
</html>
