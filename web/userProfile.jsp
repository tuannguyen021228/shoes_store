<%-- 
    Document   : userProfile
    Created on : Oct 3, 2022, 3:19:15 PM
    Author     : TuaniuLinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Art shoes store</title>
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- VALIDATION PLUGIN -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <!-- ICON -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
        <link rel="stylesheet" href="css/all.css"/>
        <script src="js/updateProfile.js"></script>
        <script src="js/AddressShip.js"></script>
    </head>
    <body>
        <style>
            a.sideB {
                background-image: linear-gradient(
                    to right,
                    #54b3d6,
                    #54b3d6 50%,
                    #000 50%
                    );
                background-size: 200% 100%;
                background-position: -100%;
                display: inline-block;
                padding: 5px 0;
                position: relative;
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
                transition: all 0.3s ease-in-out;
            }

            a.sideB:before{
                content: '';
                background: #54b3d6;
                display: block;
                position: absolute;
                bottom: -3px;
                left: 0;
                width: 0;
                height: 3px;
                transition: all 0.3s ease-in-out;
            }

            a.sideB:hover {
                background-position: 0;
            }

            a.sideB:hover::before{
                width: 100%;
            }
            .error {
                color: red;
                padding-top: 10px;
            }
        </style>
        <script type="text/javascript">
            function deleteAddress(id) {
                if (confirm("Are you sure to delete ship address with id: " + id)) {
                    window.location = "DeleteAddressShip?id=" + id;
                }
            }
            function deleteMyOrder(id) {
                if (confirm("Are you sure to delete order with id: " + id)) {
                    window.location = "DeleteOrder?id=" + id;
                }
            }
        </script>
        <jsp:include page="header.jsp"/>
        <div class="row" style="margin: 0">
            <div class="col-3 shadow">
                <div class="d-flex justify-content-center mt-5 pt-5">
                    <img class="rounded-circle img-thumbnail" style="height: 150px; width: 150px;" src="https://i.pinimg.com/736x/fd/aa/9d/fdaa9db62b7da804a40845083e5f368a.jpg" alt="">
                </div>
                <div class="d-flex justify-content-center mt-4">
                    <h5 class="fw-bold"> ${sessionScope.account.userName}</h5>
                </div>
                <div class="d-flex justify-content-center my-4">
                    <h6>Register Date: ${sessionScope.account.registerDate}</h6>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="UserProfile?action=view" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionUserProfile eq 'view' or sessionScope.actionUserProfile eq null}">fw-bold</c:when>
                       </c:choose> sideB">View information</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="UserProfile?action=update" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionUserProfile eq 'update'}">fw-bold</c:when>
                       </c:choose> sideB">Update information</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="AddressShip" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionUserProfile eq 'shipAddress'}">fw-bold</c:when>
                       </c:choose> sideB">Add ship address</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="ViewOrder" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionUserProfile eq 'viewOrder'}">fw-bold</c:when>
                       </c:choose> sideB">Order</a>
                </div>
                <hr>
            </div>
            <div class="col-9 py-4 my-1">
                <!--BEGIN COL9-->
                <div class="card mx-5 my-5 border-secondary">
                    <h4 class="card-header">
                        <c:choose><c:when test="${sessionScope.actionUserProfile eq 'view'}"><p class="mt-3 mx-4">YOUR PROFILE</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionUserProfile eq 'update'}"><p class="mt-3 mx-4">UPDATE PROFILE</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionUserProfile eq 'shipAddress'}"><p class="mt-3 mx-4">ADDRESS FOR SHIPPING</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionUserProfile eq 'viewOrder'}"><p class="mt-3 mx-4">ORDER</p></c:when></c:choose>
                            </h4>
                            <div class="card-body">
                        <c:choose>
                            <c:when test="${sessionScope.actionUserProfile eq 'view'}">
                                <c:choose>
                                    <c:when test="${sessionScope.account.isUpdateInformation == true}">
                                        <div class="mx-4">
                                            <div class="mb-4 mt-3 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="email">Email:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="email" name="email" value="${sessionScope.account.email}" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="userName">Username:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="userName" name="userName" value="${sessionScope.account.userName}" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="fullName">Full name:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="fullName" name="fullName" value="${sessionScope.account.fullName}" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="address">Address:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="address" name="address" value="${sessionScope.account.address}" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="phoneNumber">Phone number:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="phoneNumber" name="phoneNumber" value="${sessionScope.account.phoneNumber}" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="gender">Gender:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="gender" name="gender" value="${sessionScope.account.gender}" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="mx-4">
                                            <div class="mb-4 mt-3 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="email">Email:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="email" name="email" value="Empty" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="userName">Username:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="userName" name="userName" value="${sessionScope.account.userName}" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="fullName">Full name:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="fullName" name="fullName" value="Empty" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="address">Address:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="address" name="address" value="Empty" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="phoneNumber">Phone number:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="phoneNumber" name="phoneNumber" value="Empty" readonly>
                                                </div>
                                            </div>
                                            <hr>
                                            <div class="mb-4 row">
                                                <label class="col-sm-2 col-form-label fw-bold" for="gender">Gender:</label>
                                                <div class="col-sm-10">
                                                    <input class="form-control-plaintext" type="text" id="gender" name="gender" value="Empty" readonly>
                                                </div>
                                            </div>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.actionUserProfile eq 'update'}">
                                <form class="mx-4" id="form_update_profile" action="UserProfile" method="post">
                                    <div class="mb-5 mt-3 row">
                                        <label for="email" class="col-sm-2 col-form-label fw-bold">Email:</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="email" name="email">
                                        </div>
                                    </div>
                                    <div class="mb-4 row">
                                        <label for="gender" class="col-sm-2 col-form-label fw-bold">Gender:</label>
                                        <div class="col-sm-10">
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" id="inlineCheckbox1" checked name="gender" value="Male">
                                                <label class="form-check-label" for="inlineCheckbox1">Male</label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input" type="radio" id="inlineCheckbox2" name="gender" value="Female">
                                                <label class="form-check-label" for="inlineCheckbox2">Female</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mb-5 row">
                                        <label for="fullName" class="col-sm-2 col-form-label fw-bold">Full name:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="fullName" name="fullName">
                                        </div>
                                    </div>
                                    <div class="mb-5 row">
                                        <label for="address" class="col-sm-2 col-form-label fw-bold">Address:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="address" name="address">
                                        </div>
                                    </div>
                                    <div class="mb-5 row">
                                        <label for="phoneNumber" class="col-sm-2 col-form-label fw-bold">Phone number:</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control" id="phoneNumber" name="phoneNumber">
                                        </div>
                                    </div>
                                    <div class="float-end mb-3">
                                        <button id="updateProfile" type="submit" class="btn btn-lg btn-dark">Update</button>
                                    </div>
                                </form>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.actionUserProfile eq 'shipAddress'}">
                                <form class="mx-5" id="form_address" action="AddressShip" method="post">
                                    <div class="mb-5 mt-3 row">
                                        <label for="addressName" class="col-sm-4 col-form-label fw-bold">Enter your address for shipping: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="addressName" name="addressName">
                                        </div>
                                    </div>
                                    <div class="float-end mb-3">
                                        <button id="addAddressShip" type="submit" class="btn btn-lg btn-dark">Add address</button>
                                    </div>
                                </form>
                            </c:when>
                        </c:choose>
                        <!------------------------------------------------- ORDER ------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionUserProfile eq 'viewOrder'}">
                                <div class="mx-5 my-5">
                                    <c:choose>
                                        <c:when test="${sessionScope.listOrders != null}">
                                            <table class="table table-bordered border-primary table-striped table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Order ID</th>
                                                        <th scope="col">Address</th>
                                                        <th scope="col">Total price</th>
                                                        <th scope="col">Order date</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${sessionScope.listOrders}" var="a" varStatus="count">
                                                        <tr>
                                                            <th scope="row">${count.index+1}</th>
                                                            <td><a class="text-decoration-none text-dark" href="#">${a.orderId}</a></td>
                                                            <td>${a.addressShip.addressName}</td>
                                                            <td>$${a.totalPrice}</td>
                                                            <td>${a.orderDate}</td>
                                                            <td><a href="#" onclick="deleteMyOrder('${a.orderId}')"><button class="btn btn-dark">Delete</button></a></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="mt-5 mx-5 text-center"><h2>List is Empty!</h2></div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:when>
                        </c:choose>
                    </div>
                </div>
                <c:choose>
                    <c:when test="${sessionScope.actionUserProfile eq 'shipAddress'}">
                        <div class="mx-5" style="margin-bottom: 250px">
                            <c:choose>
                                <c:when test="${sessionScope.listAddress != null}">
                                    <table class="table table-bordered border-primary table-striped table-hover text-center">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Address name</th>
                                                <th scope="col">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${sessionScope.listAddress}" var="a" varStatus="count">
                                                <tr>
                                                    <th scope="row">${count.index+1}</th>
                                                    <td>${a.addressName}</td>
                                                    <td><a href="#" onclick="deleteAddress('${a.addressId}')"><button class="btn btn-dark">Delete</button></a></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </c:when>
                                <c:otherwise>
                                    <div class="mx-5" style="padding-bottom: 58px"></div>
                                </c:otherwise>
                            </c:choose>
                        </div>
                    </c:when>
                </c:choose>
                <!--END COL9-->
            </div>
    </body>
</html>
