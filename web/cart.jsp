<%-- 
    Document   : cart
    Created on : Oct 31, 2022, 12:32:00 AM
    Author     : TuaniuLinh
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- VALIDATION PLUGIN -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <!-- ICON -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
        <link rel="stylesheet" href="css/all.css"/>
        <title>Art shoes store</title>
    </head>
    <body>
        <script type="text/javascript">
            function deleteItem(id) {
                if (confirm("Are you sure to delete item with id: " + id)) {
                    window.location = "DeleteItem?id=" + id;
                }
            }
        </script>
        <jsp:include page="header.jsp"/>
        <div class="row" style="margin: 0">
            <div class="col-8">
                <div class="card mx-2 my-5">
                    <div class="card-header">
                        <h2 class="my-3 mx-4">Cart shopping</h2>
                    </div>
                    <div class="card-body">
                        <c:choose>
                            <c:when test="${sessionScope.itemsSize > 0}">
                                <c:forEach items="${sessionScope.items}" var="a">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th scope="col">Product</th>
                                                <th scope="col">Name</th>
                                                <th scope="col">Color</th>
                                                <th scope="col">Size</th>
                                                <th scope="col">Quantity</th>
                                                <th scope="col">Price</th>
                                                <th scope="col">Delete</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <th scope="row"><img class="img-thumbnail" style="width: 150px; height: 200px" src="${a.shoes.image}" alt="productIMG"/></th>
                                                <td style="padding-top: 100px">${a.shoes.shoesName}</td>
                                                <td style="padding-top: 100px">${a.color.colorName}</td>
                                                <td style="padding-top: 100px">${a.size.sizes}</td>
                                                <td style="padding-top: 100px">${a.quantity}</td>
                                                <td style="padding-top: 100px" style="display: flex">
                                                    <div id="shoesPrice">$${a.shoes.price * a.quantity}</div>
                                                </td>
                                                <td style="padding-top: 100px"><a href="#" onclick="deleteItem('${a.cartId}')"><button class="btn btn-dark">Delete</button></a></td>
                                            </tr>
                                        </c:forEach>
                                    </c:when>
                                    <c:otherwise>
                                    <div class="text-center my-5">
                                        <h2>
                                            List order empty
                                        </h2>
                                    </div>
                                </c:otherwise>
                            </c:choose>
                            </tbody>
                        </table>
                    </div>
                </div>



            </div>
            <div class="col-4 bg-dark text-light">
                <form action="OrderShoes" method="get">
                    <h3 class="my-4 mx-4">Summary</h3>
                    <hr>
                    <div class="my-4 mx-4">
                        <div class="input-group mb-3">
                            <input type="text" id="code" class="form-control" placeholder="Promotion code">
                            <button class="btn btn-outline-secondary" id="button-addon2">Submit code</button>
                        </div>
                    </div>
                    <hr>
                    <div class="my-4 mx-4 row">
                        <div class="col-8">
                            Subtotal ($):
                        </div>
                        <div class="col-4">
                            <div class="float-end">$${sessionScope.total}</div>
                        </div>
                    </div>
                    <hr>
                    <div class="my-4 mx-4">
                        <select class="form-select text-center" aria-label=".form-select-lg example" id="addressId" name="addressId">
                            <option selected>Address</option>
                            <c:forEach items="${sessionScope.addressShips}" var="a">
                                <option class="py-2" value="${a.addressId}">${a.addressName}</option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="my-3 mx-4 row">
                        <div class="col-8">
                            Tax:
                        </div>
                        <div class="col-4">
                            <div class="float-end">$---</div>
                        </div>
                    </div>
                    <hr>
                    <div class="my-4 mx-4 row">
                        <div class="col-8">
                            <h3>TOTAL</h3>
                        </div>
                        <div class="col-4">
                            <div class="float-end">$${sessionScope.total}</div>
                        </div>
                    </div>
                    <div class="text-center mb-5">
                        <button style="padding: 20px 150px" type="submit" class="btn btn-lg btn-success">Check out</button>
                        <h4 class="my-4">OR</h4>
                        <button style="padding: 20px 150px" class="btn btn-lg btn-light">Check out with Paypal</button>
                    </div>
                </form>
            </div>
        </div>
    </body>
</html>
