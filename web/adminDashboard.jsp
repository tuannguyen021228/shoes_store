<%-- 
    Document   : adminDashboard
    Created on : Oct 11, 2022, 7:50:42 AM
    Author     : TuaniuLinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- VALIDATION PLUGIN -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <!-- ICON -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
        <link rel="stylesheet" href="css/all.css"/>
        <script src="js/colorJS.js"></script>
        <script src="js/brandJS.js"></script>
        <script src="js/sizeJS.js"></script>
        <script src="js/categoryJS.js"></script>

        <title>Art shoes store</title>
    </head>
    <body>
        <style>
            a.sideB {
                background-image: linear-gradient(
                    to right,
                    #54b3d6,
                    #54b3d6 50%,
                    #000 50%
                    );
                background-size: 200% 100%;
                background-position: -100%;
                display: inline-block;
                padding: 5px 0;
                position: relative;
                -webkit-background-clip: text;
                -webkit-text-fill-color: transparent;
                transition: all 0.3s ease-in-out;
            }

            a.sideB:before{
                content: '';
                background: #54b3d6;
                display: block;
                position: absolute;
                bottom: -3px;
                left: 0;
                width: 0;
                height: 3px;
                transition: all 0.3s ease-in-out;
            }

            a.sideB:hover {
                background-position: 0;
            }

            a.sideB:hover::before{
                width: 100%;
            }
            .error {
                color: red;
                padding-top: 10px;
            }
        </style>
        <script type="text/javascript">
            function deleteColor(id) {
                if (confirm("Are you sure to delete color with id: " + id)) {
                    window.location = "DeleteColor?id=" + id;
                }
            }
            function deleteSize(id) {
                if (confirm("Are you sure to delete size with id: " + id)) {
                    window.location = "DeleteSize?id=" + id;
                }
            }
            function deleteBrand(id) {
                if (confirm("Are you sure to delete brand with id: " + id)) {
                    window.location = "DeleteBrand?id=" + id;
                }
            }
            function deleteCategory(id) {
                if (confirm("Are you sure to delete category with id: " + id)) {
                    window.location = "DeleteCategory?id=" + id;
                }
            }
        </script>
        <jsp:include page="header.jsp"/>
        <div class="row" style="margin: 0">
            <div class="col-2 shadow">
                <div class="my-4 ms-3">
                    <a href="Statistic" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'statistic' or sessionScope.actionAdminDashboard eq null}">fw-bold</c:when>
                       </c:choose> sideB">Statistical</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="viewOrderAdmin" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'viewOrder'}">fw-bold</c:when>
                       </c:choose> sideB">View Order</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="AdminDashboard?action=viewAccountMember" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'viewAccountMember'}">fw-bold</c:when>
                       </c:choose> sideB">View member account</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="AddProduct" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'addProduct'}">fw-bold</c:when>
                       </c:choose> sideB">Add product</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="ViewProduct" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'viewProduct'}">fw-bold</c:when>
                       </c:choose> sideB">View & edit product</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="AdminDashboard?action=addImg" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'addImg'}">fw-bold</c:when>
                       </c:choose> sideB">Add images</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="Category" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'addCategory'}">fw-bold</c:when>
                       </c:choose> sideB">Add categories</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="Brand" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'addBrand'}">fw-bold</c:when>
                       </c:choose> sideB">Add Brand</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="Size" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'addSize'}">fw-bold</c:when>
                       </c:choose> sideB">Add Size</a>
                </div>
                <hr>
                <div class="my-4 ms-3">
                    <a href="Color" class="text-decoration-none text-dark <c:choose>
                           <c:when test="${sessionScope.actionAdminDashboard eq 'addColor'}">fw-bold</c:when>
                       </c:choose> sideB">Add Color</a>
                </div>
                <hr>
            </div>
            <div class="col-10 py-4 my-1">
                <!--BEGIN COL10-->
                <div class="card mx-5 my-5 border-secondary">
                    <h4 class="card-header">
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'statistic'}"><p class="mt-3 mx-4">STATISTIC</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'viewOrder'}"><p class="mt-3 mx-4">VIEW ORDER</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'viewAccountMember'}"><p class="mt-3 mx-4">VIEW ACCOUNT MEMBER</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'addProduct'}"><p class="mt-3 mx-4">ADD PRODUCT</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'viewProduct'}"><p class="mt-3 mx-4">VIEW PRODUCT</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'addImg'}"><p class="mt-3 mx-4">ADD IMAGE DESCRIPTION</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'addCategory'}"><p class="mt-3 mx-4">ADD CATEGORY</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'addBrand'}"><p class="mt-3 mx-4">ADD BRAND</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'addSize'}"><p class="mt-3 mx-4">ADD SIZE</p></c:when></c:choose>
                        <c:choose><c:when test="${sessionScope.actionAdminDashboard eq 'addColor'}"><p class="mt-3 mx-4">ADD COLOR</p></c:when></c:choose>
                            </h4>
                            <div class="card-body">
                                <!------------------------------------------------- View Statistic -------------------------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'statistic'}">
                                <div class="mx-5 my-4">
                                    <form action="Statistic" method="get">
                                        <div class="input-group mb-3">
                                            <input type="date" class="form-control" name="dateFrom">
                                            <input type="date" class="form-control" name="dateTo">
                                            <button type="submit" class="btn btn-dark"> Search </button>
                                        </div>
                                    </form>
                                    <table class="table table-bordered border-primary table-striped table-hover text-center">
                                        <thead>
                                            <tr>
                                                <th scope="col">#</th>
                                                <th scope="col">Account id</th>
                                                <th scope="col">Username</th>
                                                <th scope="col">Register date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <c:forEach items="${sessionScope.statisticAccountList}" var="a" varStatus="count">
                                                <tr>
                                                    <th scope="row">${count.index+1}</th>
                                                    <td>${a.accountId}</td>
                                                    <td>${a.userName}</td>
                                                    <td>${a.registerDate}</td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </table>
                                </div>
                            </c:when>
                        </c:choose>
                        <!------------------------------------------------- View account list -------------------------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'viewAccountMember'}">
                                <div class="mx-5 my-4">
                                    <c:choose>
                                        <c:when test="${sessionScope.listAccount != null}">
                                            <table class="table table-bordered border-primary table-striped table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">User name</th>
                                                        <th scope="col">Full name</th>
                                                        <th scope="col">Email</th>
                                                        <th scope="col">Address</th>
                                                        <th scope="col">phone number</th>
                                                        <th scope="col">Gender</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${sessionScope.listAccount}" var="a" varStatus="count">
                                                        <tr>
                                                            <th scope="row">${count.index+1}</th>
                                                            <td>${a.userName}</td>
                                                            <c:choose>
                                                                <c:when test="${a.isUpdateInformation == true}">
                                                                    <td>${a.fullName}</td>
                                                                    <td>${a.email}</td>
                                                                    <td>${a.address}</td>
                                                                    <td>${a.phoneNumber}</td>
                                                                    <td>${a.gender}</td>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <td>Empty</td>
                                                                    <td>Empty</td>
                                                                    <td>Empty</td>
                                                                    <td>Empty</td>
                                                                    <td>Empty</td>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="mx-5 my-4 text-center">
                                                <h4 class="fw-bold">List account is empty!</h4>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:when>
                        </c:choose>   
                        <!------------------------------------------------- View order details -------------------------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'viewOrder'}">
                                <div class="mx-5 my-4">
                                    <c:choose>
                                        <c:when test="${sessionScope.listOrderDetails != null}">
                                            <table class="table table-bordered border-primary table-striped table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Order id</th>
                                                        <th scope="col">Shoes name</th>
                                                        <th scope="col">quantity</th>
                                                        <th scope="col">Unit price</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${sessionScope.listOrderDetails}" var="a" varStatus="count">
                                                        <tr>
                                                            <th scope="row">${count.index+1}</th>
                                                            <td>${a.orderId}</td>
                                                            <td>${a.shoes.shoesName}</td>
                                                            <td>${a.quantity}</td>
                                                            <td>${a.unitPrice}</td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="mx-5 my-4 text-center">
                                                <h4 class="fw-bold">List account is empty!</h4>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:when>
                        </c:choose>
                        <!------------------------------------------------- View shoes list -------------------------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'viewProduct'}">
                                <div class="mx-5 my-4">
                                    <c:choose>
                                        <c:when test="${sessionScope.listShoes != null}">
                                            <table class="table table-bordered border-primary table-striped table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Shoes name</th>
                                                        <th scope="col">Price</th>
                                                        <th scope="col">Delete</th>
                                                        <th scope="col">Update</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${sessionScope.listShoes}" var="a" varStatus="count">
                                                        <tr>
                                                            <th scope="row">${count.index+1}</th>
                                                            <td>${a.shoesName}</td>
                                                            <td>${a.price}</td>
                                                            <td><a href="#"><button class="btn btn-dark">Delete</button></a></td>
                                                            <td><a href="#"><button class="btn btn-dark">Update</button></a></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:when>
                                        <c:otherwise>
                                            <div class="mx-5 my-4 text-center">
                                                <h4 class="fw-bold">List shoes is empty!</h4>
                                            </div>
                                        </c:otherwise>
                                    </c:choose>
                                </div>
                            </c:when>
                        </c:choose>
                        <!------------------------------------------------- ADD PRODUCT ---------------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addProduct'}">
                                <c:choose>
                                    <c:when test="${sessionScope.errorProduct != null}">
                                        <p class="text-danger text-center">Error: ${sessionScope.errorProduct}</p>
                                    </c:when>
                                </c:choose>
                                <form class="mx-5" id="form_product" action="AddProduct" method="post">
                                    <div class="mb-5 mt-3 row">
                                        <label for="shoesName" class="col-sm-4 col-form-label fw-bold">Enter shoes name: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="shoesName" name="shoesName">
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label for="price" class="col-sm-4 col-form-label fw-bold">Enter price for shoes: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="price" name="price">
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label for="quantity" class="col-sm-4 col-form-label fw-bold">Enter quantity all color: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="quantity" name="quantity">
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label for="description" class="col-sm-4 col-form-label fw-bold">Enter description for shoes: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="description" name="description">
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label for="image" class="col-sm-4 col-form-label fw-bold">Enter url image for shoes: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="image" name="image">
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label for="categoryId" class="col-sm-4 col-form-label fw-bold">Choose category for shoes: </label>
                                        <div class="col-sm-8">
                                            <select class="form-select text-center" aria-label=".form-select-lg example" id="categoryId" name="categoryId">
                                                <option selected>Categories</option>
                                                <c:forEach items="${sessionScope.categories}" var="a">
                                                    <option class="py-2" value="${a.categoryId}">${a.categoryName}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label for="brandId" class="col-sm-4 col-form-label fw-bold">Choose brand for shoes: </label>
                                        <div class="col-sm-8">
                                            <select class="form-select text-center" aria-label=".form-select-lg example" id="brandId" name="brandId">
                                                <option selected>Brand</option>
                                                <c:forEach items="${sessionScope.brands}" var="a">
                                                    <option class="py-2" value="${a.brandId}">${a.brandName}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label for="generationId" class="col-sm-4 col-form-label fw-bold">Choose generation for shoes: </label>
                                        <div class="col-sm-8">
                                            <select class="form-select text-center" aria-label=".form-select-lg example" id="generationId" name="generationId">
                                                <option selected>Generation</option>
                                                <c:forEach items="${sessionScope.generations}" var="a">
                                                    <option class="py-2" value="${a.generationId}">${a.generationName}</option>
                                                </c:forEach>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label class="col-sm-4 col-form-label fw-bold">Choose colors for shoes:</label>
                                        <div class="col-sm-8">
                                            <c:forEach items="${sessionScope.colors}" var="a" varStatus="count">
                                                <div class="form-check form-check-inline mb-2">
                                                    <input class="form-check-input" type="checkbox" id="colorId${count.index+1}" value="${a.colorId}" name="colorId">
                                                    <label class="form-check-label" for="colorId${count.index+1}">${a.colorName}                                                    
                                                    </label>
                                                    <div style="background-color: ${a.colorName}; width: 15px; height: 15px; display: inline-block;" class="border border-secondary ms-1 rounded-circle"></div>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label class="col-sm-4 col-form-label fw-bold">Choose size for shoes:</label>
                                        <div class="col-sm-8">
                                            <c:forEach items="${sessionScope.sizes}" var="a" varStatus="count">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="sizeId${count.index+1}" value="${a.sizeId}" name="sizeId">
                                                    <label class="form-check-label" for="sizeId${count.index+1}">${a.sizes}</label>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="mb-5 mt-3 row">
                                        <label class="col-sm-4 col-form-label fw-bold">Choose gender for shoes:</label>
                                        <div class="col-sm-8">
                                            <c:forEach items="${sessionScope.genders}" var="a" varStatus="count">
                                                <div class="form-check form-check-inline">
                                                    <input class="form-check-input" type="checkbox" id="genderId${count.index+1}" value="${a.genderId}" name="genderId">
                                                    <label class="form-check-label" for="genderId${count.index+1}">${a.genderName}</label>
                                                </div>
                                            </c:forEach>
                                        </div>
                                    </div>
                                    <div class="float-end mb-3">
                                        <button id="productBtn" type="submit" class="btn btn-lg btn-dark">Add Product</button>
                                    </div>
                                </form>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addProduct'}">
                                <div class="mx-5 my-4">
                                    <c:choose>
                                        <c:when test="${sessionScope.listProduct != null}">
                                            <table class="table table-bordered border-primary table-striped table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Category name</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${sessionScope.listProduct}" var="a" varStatus="count">
                                                        <tr>
                                                            <th scope="row">${count.index+1}</th>
                                                            <td>${a.categoryName}</td>
                                                            <td><a href="#" onclick="deleteCategory('${a.categoryId}')"><button class="btn btn-dark">Delete</button></a></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </c:when>
                        </c:choose>
                        <!------------------------------------------------- ADD COLOR ---------------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addColor'}">
                                <c:choose>
                                    <c:when test="${sessionScope.errorColor != null}">
                                        <p class="text-danger text-center">Error: ${sessionScope.errorColor}</p>
                                    </c:when>
                                </c:choose>
                                <form class="mx-5" id="form_color" action="Color" method="post">
                                    <div class="mb-5 mt-3 row">
                                        <label for="colorName" class="col-sm-4 col-form-label fw-bold">Enter color name for add color: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="colorName" name="colorName">
                                        </div>
                                    </div>
                                    <div class="float-end mb-3">
                                        <button id="colorBtn" type="submit" class="btn btn-lg btn-dark">Add Color</button>
                                    </div>
                                </form>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addColor'}">
                                <div class="mx-5 my-4">
                                    <c:choose>
                                        <c:when test="${sessionScope.listColor != null}">
                                            <table class="table table-bordered border-primary table-striped table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Color name</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${sessionScope.listColor}" var="a" varStatus="count">
                                                        <tr>
                                                            <th scope="row">${count.index+1}</th>
                                                            <td>${a.colorName}</td>
                                                            <td><a href="#" onclick="deleteColor('${a.colorId}')"><button class="btn btn-dark">Delete</button></a></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </c:when>
                        </c:choose>
                        <!------------------------------------------------- ADD SIZE ---------------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addSize'}">
                                <c:choose>
                                    <c:when test="${sessionScope.errorSize != null}">
                                        <p class="text-danger text-center">Error: ${sessionScope.errorSize}</p>
                                    </c:when>
                                </c:choose>
                                <form class="mx-5" id="form_size" action="Size" method="post">
                                    <div class="mb-5 mt-3 row">
                                        <label for="sizes" class="col-sm-4 col-form-label fw-bold">Enter sizes for add size: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="sizes" name="sizes">
                                        </div>
                                    </div>
                                    <div class="float-end mb-3">
                                        <button id="sizeBtn" type="submit" class="btn btn-lg btn-dark">Add Size</button>
                                    </div>
                                </form>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addSize'}">
                                <div class="mx-5 my-4">
                                    <c:choose>
                                        <c:when test="${sessionScope.listSize != null}">
                                            <table class="table table-bordered border-primary table-striped table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Sizes</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${sessionScope.listSize}" var="a" varStatus="count">
                                                        <tr>
                                                            <th scope="row">${count.index+1}</th>
                                                            <td>${a.sizes}</td>
                                                            <td><a href="#" onclick="deleteSize('${a.sizeId}')"><button class="btn btn-dark">Delete</button></a></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </c:when>
                        </c:choose>
                        <!------------------------------------------------- ADD BRAND ---------------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addBrand'}">
                                <c:choose>
                                    <c:when test="${sessionScope.errorBrand != null}">
                                        <p class="text-danger text-center">Error: ${sessionScope.errorBrand}</p>
                                    </c:when>
                                </c:choose>
                                <form class="mx-5" id="form_brand" action="Brand" method="post">
                                    <div class="mb-5 mt-3 row">
                                        <label for="brandName" class="col-sm-4 col-form-label fw-bold">Enter name for add brand: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="brandName" name="brandName">
                                        </div>
                                    </div>
                                    <div class="float-end mb-3">
                                        <button id="brandBtn" type="submit" class="btn btn-lg btn-dark">Add Brand</button>
                                    </div>
                                </form>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addBrand'}">
                                <div class="mx-5 my-4">
                                    <c:choose>
                                        <c:when test="${sessionScope.listBrand != null}">
                                            <table class="table table-bordered border-primary table-striped table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Brand name</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${sessionScope.listBrand}" var="a" varStatus="count">
                                                        <tr>
                                                            <th scope="row">${count.index+1}</th>
                                                            <td>${a.brandName}</td>
                                                            <td><a href="#" onclick="deleteBrand('${a.brandId}')"><button class="btn btn-dark">Delete</button></a></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </c:when>
                        </c:choose>
                        <!------------------------------------------------- ADD CATEGORY ---------------------------------------------------------->
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addCategory'}">
                                <c:choose>
                                    <c:when test="${sessionScope.errorCategory != null}">
                                        <p class="text-danger text-center">Error: ${sessionScope.errorCategory}</p>
                                    </c:when>
                                </c:choose>
                                <form class="mx-5" id="form_category" action="Category" method="post">
                                    <div class="mb-5 mt-3 row">
                                        <label for="categoryName" class="col-sm-4 col-form-label fw-bold">Enter name for add category: </label>
                                        <div class="col-sm-8">
                                            <input type="text" class="form-control" id="categoryName" name="categoryName">
                                        </div>
                                    </div>
                                    <div class="float-end mb-3">
                                        <button id="categoryBtn" type="submit" class="btn btn-lg btn-dark">Add Category</button>
                                    </div>
                                </form>
                            </c:when>
                        </c:choose>
                        <c:choose>
                            <c:when test="${sessionScope.actionAdminDashboard eq 'addCategory'}">
                                <div class="mx-5 my-4">
                                    <c:choose>
                                        <c:when test="${sessionScope.listCategory != null}">
                                            <table class="table table-bordered border-primary table-striped table-hover text-center">
                                                <thead>
                                                    <tr>
                                                        <th scope="col">#</th>
                                                        <th scope="col">Category name</th>
                                                        <th scope="col">Delete</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <c:forEach items="${sessionScope.listCategory}" var="a" varStatus="count">
                                                        <tr>
                                                            <th scope="row">${count.index+1}</th>
                                                            <td>${a.categoryName}</td>
                                                            <td><a href="#" onclick="deleteCategory('${a.categoryId}')"><button class="btn btn-dark">Delete</button></a></td>
                                                        </tr>
                                                    </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:when>
                                    </c:choose>
                                </div>
                            </c:when>
                        </c:choose>
                    </div>
                </div>


            </div>
            <!--END COL10-->
        </div>
    </body>
</html>
