<%-- 
    Document   : home
    Created on : Sep 28, 2022, 11:27:00 AM
    Author     : TuaniuLinh
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page isELIgnored="false"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- JS, Popper.js, and jQuery -->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

        <!-- VALIDATION PLUGIN -->
        <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
        <!-- ICON -->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.1.2/css/all.min.css" />
        <link rel="stylesheet" href="css/all.css"/>

        <title>Art shoes store</title>
    </head>

    <body>
        <jsp:include page="header.jsp"/>
        <div class="mt-4 ms-3 ps-5">
            <h3>Shoes & Sneakers (${sessionScope.sizeListShoes})</h3>
        </div>
        <div class="row mt-4" style="margin: 0;">
            <div class="col-2 ms-5" id="bar">
                <div>
                    <div>
                        <h5 class="fw-bold mb-4">Categories</h5>
                    </div>
                    <c:forEach items="${sessionScope.categories}" var="a">
                        <div>
                            <a class="nav-link my-2" href="homeCategory?categoryId=${a.categoryId}">${a.categoryName}</a>
                        </div>
                    </c:forEach>
                </div>
                <hr>
                <div>
                    <h5 class="fw-bold mb-4">Gender</h5>
                    <div>
                        <a class="nav-link my-2" href="homeGender?gender=Male">Male</a>
                    </div>
                    <div>
                        <a class="nav-link my-2" href="homeGender?gender=Female">Female</a>
                    </div>
                    <div>
                        <a class="nav-link my-2" href="homeGender?gender=Unisex">Unisex</a>
                    </div>
                </div>
                <hr>
                <div>
                    <h5 class="fw-bold mb-4">Price</h5>
                    <div>
                        <a class="nav-link my-2" href="homePrice?priceFrom=0&priceTo=100">Under $100</a>
                    </div>
                    <div>
                        <a class="nav-link my-2" href="homePrice?priceFrom=100&priceTo=200">$100 - $200</a>
                    </div>
                    <div>
                        <a class="nav-link my-2" href="homePrice?priceFrom=200&priceTo=300">$200 - $300</a>
                    </div>
                    <div>
                        <a class="nav-link my-2" href="homePrice?priceFrom=400&priceTo=500">$400 - $500</a>
                    </div>
                    <div>
                        <a class="nav-link my-2" href="homePrice?priceFrom=500&priceTo=-1">Over $500</a>
                    </div>
                </div>
                <hr>
                <div>
                    <h5 class="fw-bold mb-4">Sort by name</h5>
                    <div>
                        <a class="nav-link my-2" href="homeSortName?sort=ASC">A - Z</a>
                    </div>
                    <div>
                        <a class="nav-link my-2" href="homeSortName?sort=DESC">Z - A</a>
                    </div>
                </div>
                <hr>
                <div>
                    <h5 class="fw-bold mb-4">Sort by price</h5>
                    <div>
                        <a class="nav-link my-2" href="homeSortPrice?sort=ASC">Ascending</a>
                    </div>
                    <div>
                        <a class="nav-link my-2" href="homeSortPrice?sort=DESC">Descending</a>
                    </div>
                </div>
            </div>
            <div id="shoes" class="col-9 ps-5 ms-4" style="margin: 0;">
                <div class="row">
                    <c:forEach items="${sessionScope.listShoesHome}" var="a">
                        <a class="mb-4 col-4 text-decoration-none text-dark" href="shoesDetail?shoesId=${a.shoesId}&colorId=0&sizeId=0">
                            <div>
                                <img style="height: 500px; width: 400px;" src="${a.image}" alt="">
                            </div>
                            <h6 class="mt-3 fw-bold">${a.shoesName}</h6>
                            <p class="mt-2">${a.category.categoryName}</p>
                            <p>$${a.price}</p>
                        </a>
                    </c:forEach>

                </div>

            </div>
            <c:if test="${sessionScope.index > 0}">
                <div class="row">
                    <div class="col-3" style="margin: 0;">

                    </div>
                    <div class="col-9 d-flex justify-content-center" style="margin: 0;">
                        <nav class="text-dark">
                            <ul class="pagination">
                                <c:if test="${sessionScope.index > 1}"><li class="page-item"><a class="page-link" href="home?index=${sessionScope.index-1}">Previous</a></li></c:if>
                                    <c:forEach begin="1" end="${sessionScope.endPage}" var="i">
                                    <li class="page-item">
                                        <a class="page-link ${sessionScope.index == i ? "active" : ""}" href="home?index=${i}">${i}</a>
                                    </li>
                                </c:forEach>
                                <c:if test="${sessionScope.index < endPage}"><li class="page-item"><a class="page-link" href="home?index=${sessionScope.index+1}">Next</a></li></c:if>

                                </ul>
                            </nav>
                        </div>
                    </div>
            </c:if>
        </div>
        <h1 class="text-center my-5">Top 3 highest price</h1>
        <div class="container">
            <div class="row" style="margin: 0;">
                <c:forEach items="${sessionScope.top3highesprice}" var="a">
                    <a class="mb-4 col-4 text-decoration-none text-dark" href="shoesDetail?shoesId=${a.shoesId}&colorId=0&sizeId=0">
                        <div>
                            <img style="height: 500px; width: 400px;" src="${a.image}" alt="">
                        </div>
                        <h6 class="mt-3 fw-bold">${a.shoesName}</h6>
                        <p class="mt-2">${a.category.categoryName}</p>
                        <p>$${a.price}</p>
                    </a>
                </c:forEach>
            </div>
        </div>
        <h1 class="text-center my-5">Top 3 lowest price</h1>
        <div class="container">
            <div class="row" style="margin: 0;">
                <c:forEach items="${sessionScope.top3lowetsprice}" var="a">
                    <a class="mb-4 col-4 text-decoration-none text-dark" href="shoesDetail?shoesId=${a.shoesId}&colorId=0&sizeId=0">
                        <div>
                            <img style="height: 500px; width: 400px;" src="${a.image}" alt="">
                        </div>
                        <h6 class="mt-3 fw-bold">${a.shoesName}</h6>
                        <p class="mt-2">${a.category.categoryName}</p>
                        <p>$${a.price}</p>
                    </a>
                </c:forEach>
            </div>
        </div>
        <h1 class="text-center my-5">Top 3 sale</h1>
        <div class="container">
            <div class="row" style="margin: 0;">
                <c:forEach items="${sessionScope.top3sales}" var="a">
                    <a class="mb-4 col-4 text-decoration-none text-dark" href="shoesDetail?shoesId=${a.shoesId}&colorId=0&sizeId=0">
                        <div>
                            <img style="height: 500px; width: 400px;" src="${a.image}" alt="">
                        </div>
                        <h6 class="mt-3 fw-bold">${a.shoesName}</h6>
                        <p class="mt-2">${a.category.categoryName}</p>
                        <p>$${a.price}</p>
                    </a>
                </c:forEach>
            </div>
        </div>
        <div class="text-center mt-5 bg-dark">
            <div class="py-5">
                <p class="text-white">Copyright © 2022 ArtShoeStore Inc. All rights reserved.</p>
                <p class="text-white">Developer: TuanNB</p>
                <div class="mt-4">
                    <a href="https://www.facebook.com/tuannguyen281202/"><i style="font-size: 40px;" class="fa-brands fa-facebook"></i></a>
                    <a class="ms-5" href="https://www.instagram.com/kint.eagle/"><i style="font-size: 40px;" class="fa-brands fa-instagram"></i></a>
                    <a class="ms-5" href="https://twitter.com/k16_hl"><i style="font-size: 40px;" class="fa-brands fa-twitter"></i></a>
                </div> 
            </div>

        </div>
    </body>

</html>
