/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */


$(document).ready(function() {
    $(document).on("click", "#register", function() {
        $("#form_jquery_reg").validate({
            rules: {
                userName: {
                    required: true,
                    minlength: 4,
                    maxlength: 16
                },
                password: {
                    required: true,
                    minlength: 8,
                    maxlength: 16
                },
                rePassword: {
                    required: true,
                    minlength: 8,
                    maxlength: 16,
                    equalTo: "#password"
                }
            },
            messages: {
                userName: {
                    required: "Enter your user name",
                    minlength: "You must input 4 characters at least",
                    maxlength: "Your user name must less than 16 characters"
                },
                password: {
                    required: "Enter your password",
                    minlength: "You must input 8 characters at least",
                    maxlength: "Your password must less than 16 characters"
                },
                rePassword: {
                    required: "Enter your re-password",
                    minlength: "You must input 8 characters at least",
                    maxlength: "Your re-password must less than 16 characters",
                    equalTo: "Your re-password not match"
                }
            }
        });
        $("#form_jquery_reg").valid();
    });
});

