/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
$.validator.addMethod('customphone', function(value, element) {

    return this.optional(element) || /(84|0[3|5|7|8|9])+([0-9]{8})\b/.test(value);

}, "Please input your correct phone number");
$(document).ready(function() {
    $(document).on("click", "#updateProfile", function() {
        $("#form_update_profile").validate({
            rules: {
                userName: {
                    required: true,
                    minlength: 4,
                    maxlength: 16
                },
                email: {
                    required: true,
                    maxlength: 50
                },
                fullName: {
                    required: true,
                    minlength: 4,
                    maxlength: 50
                },
                address: {
                    required: true,
                    minlength: 4,
                    maxlength: 50
                },
                phoneNumber: {
                    required: true,
                    customphone: true
                }
            },
            messages: {
                userName: {
                    required: "Enter your user name!",
                    minlength: "You must input 4 characters at least",
                    maxlength: "Your user name must less than 16 characters"
                },
                email: {
                    required: "Enter your email!",
                    maxlength: "Your email must less than 50 characters"
                },
                fullName: {
                    required: "Enter your full name!",
                    minlength: "You must input 4 characters at least",
                    maxlength: "Your full name must less than 50 characters"
                },
                address: {
                    required: "Enter your address!",
                    minlength: "You must input 4 characters at least",
                    maxlength: "Your address must less than 50 characters"
                },
                phoneNumber: {
                    required: "Enter your phone number"
                }
            }
        });
        $("#form_update_profile").valid();
    });
});

