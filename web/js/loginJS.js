$(document).ready(function() {
    $(document).on("click", "#login", function() {
        $("#form_jquery").validate({
            rules: {
                userName: {
                    required: true,
                    minlength: 4,
                    maxlength: 16
                },
                password: {
                    required: true,
                    minlength: 8,
                    maxlength: 16
                }
            },
            messages: {
                userName: {
                    required: "Enter your user name",
                    minlength: "You must input 4 characters at least",
                    maxlength: "Your user name must less than 16 characters"
                },
                password: {
                    required: "Enter your password",
                    minlength: "You must input 8 characters at least",
                    maxlength: "Your password must less than 16 characters"
                }
            }
        });
        $("#form_jquery").valid();
    });
});