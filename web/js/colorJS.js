/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/JavaScript.js to edit this template
 */
$(document).ready(function() {
    $(document).on("click", "#colorBtn", function() {
        $("#form_color").validate({
            rules: {
                colorName: {
                    required: true,
                    minlength: 2,
                    maxlength: 50
                }
            },
            messages: {
                colorName: {
                    required: "Enter color to add!",
                    minlength: "You must input 2 characters at least",
                    maxlength: "Color name must less than 50 characters"
                }
            }
        });
        $("#form_color").valid();
    });
});

